#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "interpreter.h"
#include "opcodes.h"


static inline long long GetCPUCount ()
     //inline void GetCPUCount (__int64 &dest )
{
  //unsigned int loword, hiword;
  unsigned long long outp;
  asm volatile (    
		//"xorl %%eax, %%eax \n\t"
		//"cpuid \n\t"
      "rdtsc \n\t"
      :"=A"(outp)
      :
      :"ebx", "ecx"
      /*      "movl iword , edx"
	      "mov loword , eax" */
      //:"=a"(outp), "=d"(hiword)
      //:"=a"(loword), "=d"(hiword)
      );
  return outp;
  //return ( (__int64) hiword << 32 ) + loword;
}
unsigned long long stamp_t1, stamp_t2;

#define OPCODE(label) label:
#define OPCODEDEL(label) DELG_##label: deref(gen); label:

//The type of an opcode operand specifying a register number: we shall allow
//64k or 256 registers in a script
//
//#define LOCAL_NUM_TYPE int //short
#define RARGSZ sizeof(LOCAL_NUM_TYPE)
//Not really used yet
#define PTRSZ sizeof(void *)

//#define TRACEPRINT(stuff) DPRINT(stuff)
#define TRACEPRINT(stuff) 

#define DISPATCH(amount) pc += sizeof(void *) + (amount); TRACEPRINT(("dispatching to %p %s\n", *(void **)pc, getopcname(*(void **)pc))); goto **(void **)pc
//#define DYN_DISPATCH(amount, inst_id) pc += amount; goto *inst[inst_id];
#define DYN_DISPATCH(offset) pc = ((void *)inst) + (offset); TRACEPRINT(("dyndispatching[%d] to %p %s\n", (offset), *(void **)pc, getopcname(*(void **)pc))); goto **(void **)pc

//#define SKIPARGS(amount) pc += amount;

//Pointer to the operand with offset in bytes from start of operands
//(int * by default)
#define ARG(offset) (int *)(pc + sizeof(void *) + (offset))
//Assuming that (at least the first) operands are register numbers, the register
//that the nth operand specifies
#define LOCALARG(number) (locals[*(LOCAL_NUM_TYPE *)ARG((number) * RARGSZ)])

void bcloop(int init, void **inst, int *locals) {
  static void *_opcodes[] = {
    /*opNIL*/NULL,
    &&PUSH,
    &&SET_SP,
    &&REF,
    &&DELG,
    &&DEL,
    &&DEL_REGS,
    &&REWRITE_ADDR,
    &&EXIT,
    &&JUMP,
    &&JUMPZ,
    &&JUMPNZ,
    &&FOR_INC_LTEQ_INT,
    &&FOR_INC_GTEQ_INT,
    &&DO,
    &&DO_INT,
    &&LOOP,
    &&INT,
    &&DELG_INT,
    &&MOV_INT,
    &&DOUBLE,
    &&DELG_DOUBLE,
    &&REG,
    &&DELG_REG,
    &&MOV_REG,
    &&SET_REG,
    &&SET_VAR,
    &&ENCODE_INT,
    &&ENCODE_INT_REG,
    &&DECODE_INT,
    &&ADD_INT_INT,
    &&ADD_ANY_ANY,
    &&INC_INT_INT,
    &&CAST_DOUBLE,
    &&PRINT_INT,
    &&PRINT_VAR,
    &&PRINT_TABLES,
    &&PRINT_STATE,
    &&START_TIMER,
    &&STOP_TIMER,
    &&CALL_BUILTIN_STACK,
    &&NEW_STRING,
  };

  if (init) {
    opcodes = _opcodes;
    return;
  }

  void *callstack, **callsp;

  callstack = malloc(128);
  callsp = callstack;

  int *stackp;

  stackp = locals;

  //limit on builtin commands is 32 args
  int call_typed_mask;

  void *pc;
  /*register*/ int ctr;  //DELETEME
  /*register*/ int gen;
  struct data_table *tbl;
  //int temp;
  int *tempp, *tempp2;
  int i;

  pc = (int *)&inst[0];

  TRACEPRINT(("dispatching to %p %s\n", *(void **)pc, getopcname(*(void **)pc)));
  goto **(void **)pc;

  for(;;) {

    /* NEW EDITION OPCODE GUIDE
     * =NAMING SCHEME=
     *  Undecided?
     *  MOV_*: overwrite value, assuming not typed
     *  SET_*: change value, deleting old typed value, if any
     *  DELG_*: created if an opcode is specified with OPCODEDEL instead of
     *          OPCODE; delete gen instead of considering it empty.
     *
     * =DOCUMENTATION COMMENTS SCHEME=
     *  OPCODE[DEL](OPNAME) //(arguments...):  effects...
     *  Each argument has a (possibly compound type), and a name. If the name
     *  is prefixed with $, then that arg does not appear in the bytecode stream
     *  but is retrieved from elsewhere:
     *  -$gen is the name of the special gen register variable. If an opcode
     *   does not take $gen as an argument, then it will overwrite it at will.
     *   By convention, the last argument to normal opcodes is passed in $gen.
     *  -$stackp is the stack pointer, it points into the locals array. The
     *   stack grows upwards.
     *  -$stack[...] read from locals using stackp

     *  Types:
     *   Script data types:
     *    int: a signed 32 bit int
     *    string: struct string_var*
     *    double: struct double_var*
     *    array: struct array_var*
     *    enc<type>: any of the above, encoded. Eg encint
     *    void: a variable assumed to start empty
     *    any: any type, encoded
     *   Other:
     *    long: the native word (as compiled), either 32 or 64 bits
     *   Type compounders:
     *    reg*: an register number, offset in the locals array
     *    stk*: an register number, offset to stackp. 

     *  Effects:
     *  reg* & stk* type arguments can be dereferenced with *, meaning indexing locals
     *  Deleting the old value of a register must be explicitly stated!!
     *  Operators:
     *    encode()
     *    decode(): type of the result should be known?
     *    new(): create a new object from given initialiser with refc 1, returning
     *           encoded value
     *    ref(): increment refcount, returning a new encoded handle which might be
     *           different if the data had to copied (not comfortable with this...)
     *    delete(): dec refcount of an encoded value, delete on hitting 0
     *    push()

     * =CONVENTIONS=
     *  gen wil always either be empty or contained an encoded or decoded value
     *  It may or may not be counted as a reference to a variable, that's up to the
     *  bytecode compiler to keep track of!
     */
     
    /************** Special instructions *************/

  OPCODE(PUSH) //(any $gen):  push(gen); gen = 0;
    *stackp++ = gen;
    gen = 0; //DELETEME
    DISPATCH(0);

  OPCODE(SET_SP) //(any reg* i):  stackp = i;
    stackp = &LOCALARG(0);
    DISPATCH(RARGSZ);

  OPCODE(REF) //(any $gen):  ref(gen);
    ref(gen);
    DISPATCH(0);

  OPCODE(DELG) //(any $gen):  delete(gen); gen = 0;
    deref(gen);
    gen = 0; //DELETEME
    DISPATCH(0);

  OPCODE(DEL) //(any reg* r):  delete(*r); *r = 0;
    deref(LOCALARG(0));
    LOCALARG(0) = 0;
    DISPATCH(RARGSZ);

  OPCODE(DEL_REGS) //(int num):  for i=0 to num - 1: delete(local[i]);
    for (i = 0; i < *ARG(0); i++)
      deref(locals[i]);
    DISPATCH(4);

  //OPCODE(DEL_SP) //(any stk* r):  delete(*r); *r = 0;

    /************ Optimisation instructions **********/

  OPCODE(REWRITE_ADDR) //(int val, int num, int offset...)
    //For each offset[0..num-1], write val to pc[offset[n]]] (ie, relative to the current position in bytes)
    //For the purpose of rewriting the address of some variable
    for (i = 0; i < *ARG(4); i++)
      *(int *)(pc + *ARG(i+8)) = *ARG(0);
    DISPATCH(*ARG(4) + 8);

    /************ Flow control instructions **********/

  OPCODE(EXIT) //()
    break;

  //NOTE: should jumps use short offsets instead?
  OPCODE(JUMP) //(int offset):  goto offset;
    //offset from start of instructions to jump to
    DYN_DISPATCH(*ARG(0));

  OPCODE(JUMPZ) //(int offset, int $gen):  if (!gen) goto *(pc + offset);
    //offset from start of instructions to jump to if gen is zero
    if (!gen) {
      DYN_DISPATCH(*ARG(0));
    }
    DISPATCH(4);

  OPCODE(JUMPNZ) //(int offset, int $gen):  if (gen) goto *(pc + offset);
    //offset from start of instructions to jump to if gen is nonzero
    if (gen) {
      DYN_DISPATCH(*ARG(0));
    }
    DISPATCH(4);

    /*
  //OPCODE(FOR) //(int reg* ctr, int reg* end, int reg* step, int exitjump):  if (ctr += step <=/>= end) goto exitjump;
    temp = LOCALARG(2);
    gen = LOCALARG(0) += temp;
    if (temp >= 0) {

  //OPCODE(FOR_COND_INT) //(int $gen, int reg* end, int reg* step, int repeatjump):  if (gen <=/>= end) goto repeatjump;
    if (LOCALARG(2) >= 0) {
      if (gen > LOCALARG(1)) {
	DYN_DISPATCH(*ARG(RARGSZ * 3));
      }
    } else {
      if (gen < LOCALARG(1)) {
	DYN_DISPATCH(*ARG(RARGSZ * 3));
      }
    }
    DISPATCH(RARGSZ * 3 + 4);
    */

  OPCODE(FOR_INC_LTEQ_INT) //(int reg* ctr, int reg* step, int reg* end, int repeatjump):  if (gen = *ctr += *step <= *end) goto repeatjump;
    gen = LOCALARG(0) += LOCALARG(1);
    if (gen <= LOCALARG(2)) {
      DYN_DISPATCH(*ARG(RARGSZ * 3));
    }
    DISPATCH(RARGSZ * 3 + 4);

  OPCODE(FOR_INC_GTEQ_INT) //(int reg* ctr, int reg* step, int reg* end, int repeatjump):  if (gen = *ctr += *step >= *end) goto repeatjump;
    gen = LOCALARG(0) += LOCALARG(1);
    if (gen >= LOCALARG(2)) {
      DYN_DISPATCH(*ARG(RARGSZ * 3));
    }
    DISPATCH(RARGSZ * 3 + 4);


    /*
      lab1:
      FOR_COND ... , lab2
      ...
      REG step
      INC var
      JUMP lab1
      lab2:

      REG var
      JUMP lab2
      lab1:
      ...
      REG step
      INC var
      lab2:
      FOR_COND ... , lab1
    */

    //loop some number of times, without updating a loop variable
    //number of loops in a var
  OPCODE(DO) //(int reg* looptimes)
    *callsp = pc;
    callsp++;
    ctr = LOCALARG(0);
    DISPATCH(4);

    //number of loops constant
  OPCODE(DO_INT) //(int looptimes)
    *callsp = pc;
    callsp++;
    ctr = *ARG(0);
    DISPATCH(4);

    //uses ctr as loop counter instead of a variable
    //NOTE: could replace callstack with pointer to do, then could replace branch with pc = pc[1+(ctr==0)] !
  OPCODE(LOOP) //(void* inst_after_do)
    ctr--;
    //cs--;
    //pc = *cs; 
    if (ctr) {
      //continue after DO
      pc = callsp[-1]; 
      DISPATCH(4);
    }
    else {
      callsp--;
      DISPATCH(0);
    }


    /**************** Data instructions **************/

  OPCODEDEL(INT) //(int imm):  gen = imm;
    gen = *ARG(0);
    DISPATCH(4);

  OPCODE(MOV_INT) //(void reg* r, int imm):  *r = imm;
    LOCALARG(0) = *ARG(RARGSZ);
    DISPATCH(RARGSZ + 4);

  OPCODEDEL(DOUBLE) //(double imm):  gen = new(imm);
    gen = new_table_entry(&tables[T_DOUBLE], &tempp) | FLG_DOUBLE;
    tempp[0] = 1; //refcount
    ((struct double_var *)tempp)->d = *(double *)ARG(0);
    DISPATCH(8);

    /****** Movement and encoding instructions *******/

  OPCODEDEL(REG) //(any reg* r):  gen = *r;
    gen = LOCALARG(0);
    DISPATCH(RARGSZ);

  OPCODE(MOV_REG) //(void reg* r, any $gen):  *r = gen;
    LOCALARG(0) = gen;
    DISPATCH(RARGSZ);

  OPCODE(SET_REG) //(any reg* dest, any $gen):  delete(*dest); *dest = ref(gen);
    deref(LOCALARG(0));
    LOCALARG(0) = ref(gen);
    DISPATCH(RARGSZ);

  OPCODE(SET_VAR) //(any *dest, any *src):  delete(*dest); *dest = ref(gen = *src);
    deref(*(int *)*ARG(0));

    gen = *(int *)*ARG(4);
    // This is ref() inlined
    if (VAR_TYPED(gen)) {
      tbl = &tables[VAR_TYPE(gen)];
      tempp = &tbl->p[VAR_T_ID(gen)];
      if (TYPE_REFCED(gen)) {
	/* increment reference count */
	tempp[0]++;
	*(int *)*ARG(0) = gen;
	DISPATCH(8);
      } else {
	/* duplicate data */
	*(int *)*ARG(0) = VAR_FLAGS(gen) | new_table_entry(tbl, &tempp2);
	for (i = 0; i < tables[VAR_TYPE(gen)].elmt_size; i++)
	  tempp2[i] = tempp[i];
	DISPATCH(8);
      }
    } else
      *(int *)*ARG(0) = gen;
    DISPATCH(8);

  OPCODE(ENCODE_INT) //(int $gen):  gen = encode(gen);
    if (VAR_TYPED(gen)) {
      int tempi = gen;
      gen = new_table_entry(&tables[T_LOW_INT], &tempp) | FLG_LOW_INT;
      *tempp = tempi;
    }
    DISPATCH(0);

  OPCODE(ENCODE_INT_REG) //(int reg* r):  *r = encode(*r);
    if (VAR_TYPED(LOCALARG(0))) {
      int tempi = LOCALARG(0);
      LOCALARG(0) = new_table_entry(&tables[T_LOW_INT], &tempp) | FLG_LOW_INT;
      *tempp = tempi;
    }
    DISPATCH(RARGSZ);

  OPCODE(DECODE_INT) //(encint *x):  *x = gen = decode(*x);
    gen = *(int *)*ARG(0);
    if (VAR_TYPED(gen))
      gen = *(int *)*ARG(0) = tables[T_LOW_INT].p[gen]; //pointer magic
    DISPATCH(4);

    /**************** Math operations ****************/

  OPCODE(ADD_INT_INT) //(int reg* inc, int $gen):  gen += *inc;
    gen += LOCALARG(0);
    DISPATCH(RARGSZ);

  OPCODE(ADD_ANY_ANY) //(any reg* inc, any $gen):  gen' = gen + *inc; delete(gen);
    //currently, each operand must be int or double
    {
      int i1 = LOCALARG(0), i2 = gen;
      double d1, d2;
      if (to_doubles_or_ints(&i1, &i2, &d1, &d2)) {
	d1 += d2;
	deref(gen);
	gen = new_table_entry(&tables[T_DOUBLE], &tempp) | FLG_DOUBLE;
	tempp[0] = 1; //refcount
	((struct double_var *)tempp)->d = d1;
      } else {
	deref(gen);
	gen = i1 + i2;
	//SKIPARGS(1);
	pc += 4;
	goto ENCODE_INT;
      }
    }
    DISPATCH(RARGSZ);

  OPCODE(INC_INT_INT) //(int reg* var, int $gen):  gen = *var += gen;
    gen = LOCALARG(0) += gen;
    DISPATCH(RARGSZ);

  OPCODE(CAST_DOUBLE) //(int $gen):  gen = encode((double)gen);
    {
      int tempi = gen;
      gen = new_table_entry(&tables[T_DOUBLE], &tempp) | FLG_DOUBLE;
      tempp[0] = 1; //refcount
      *(double *)&tempp[1] = (double)tempi;
    }
    DISPATCH(0);

    /************* Debugging instructions ************/

  OPCODE(PRINT_INT) //(int x):  
    scriptout("# %d\n", *ARG(0));
    DISPATCH(4);

  OPCODE(PRINT_VAR) //(any reg* r):  
    scriptout("register %d = ", *ARG(0));
    printvariable(LOCALARG(0));
    DISPATCH(RARGSZ);

  OPCODE(PRINT_TABLES) //()
    print_tables();
    DISPATCH(0);

  OPCODE(PRINT_STATE) //()
    scriptout("Printing state:\n" "gen = ");
    printvariable(gen);
    scriptout("stackp = %d\n", (int)(stackp - locals));
    print_tables();
    for (i = 0; i < stackp - locals; i++)
      scriptout("register %d: %s\n", i, valuetoa(locals[i]));
    DISPATCH(0);

  OPCODE(START_TIMER) //()
    stamp_t1 = GetCPUCount();
    DISPATCH(0);

  OPCODE(STOP_TIMER) //()
    stamp_t2 = GetCPUCount();
    DISPATCH(0);

  //OPCODE(ASSERT_INT) //(any $gen)

    /********* External interface instructions *******/

  OPCODE(CALL_BUILTIN_STACK) //(short id, short args, any $stack):  gen = int returnval; $stackp -= args;
    ////(short id, short args, any $stack[-args..-1])
    //builtins need to support typed variables to recieve them
    //So we want to enable them to recieve low ints without special support
    //we decode all low ints and set a mask for typed variables
    //It is hereby decreed that builtin commands support a maximum of 32 arguments.
    //Currently only returning ints is supported. Separate opcode for returning types? (TODO)
    //Plan: arguments should not be refc'd, since that would be a large burden for passing
    //in variables. To pass something like a string, it should be assigned to a register and
    //deleted afterwards.
    call_typed_mask = 0;
    stackp -= *(short *)ARG(2);
    for (i = 0; i < *(short *)ARG(2); i++) {
      gen = stackp[i];
      if (VAR_TYPED(gen)) {
	tbl = &tables[VAR_TYPE(gen)];
	tempp = &tbl->p[VAR_T_ID(gen)];

	if (VAR_LOW_INT(gen)) {
	  stackp[i] = *tempp;
	  *tempp = 0;
	  tbl->num--;
	} else {
	  call_typed_mask |= 1 << i;
	}
      }
    }
    //call command pc[1]
    //check all typed arguments were received
    //assume correctly received args were freed if necessary
    gen = functions_callback(*(short *)ARG(0), *(short *)ARG(2), stackp, call_typed_mask);
    DISPATCH(4);

    /***************** OBSOLETE CODE *****************/

/*VAR_POLY: //(int var_id, var *var_tbl, int *bit_tbl, int next_inst_base)
    gen = ((int *)pc[2])[pc[1]];
    if (VAR_TYPED(gen))
	  if ( ( ((int *)pc[3])[pc[1]/32] >> pc[1]%32 ) & 0x1 ) {
	    temp = VAR_TYPE(gen);
		gen = VAR_T_ID(gen);
	    // note that we increment pc to point at next_inst_base: that is, we don't bother storing a dummy jump address
	    DYN_DISPATCH(4, pc[4] + TYPED_INST_COUNT * temp);
	  }
		
	DYN_DISPATCH(4, pc[4]);
	
*/

  OPCODE(NEW_STRING) //(var reg* x, const char *c, int len):  *x = gen := new STRING(c);
    deref(LOCALARG(0));
    gen = LOCALARG(0) = new_table_entry(&tables[T_STRING], &tempp) | FLG_STRING;
    tempp[0] = 1; //refs
    tempp[1] = (int)malloc(*ARG(RARGSZ));
    tempp[2] = *ARG(RARGSZ + 4);
    memcpy((char *)tempp[1], (char *)*ARG(RARGSZ), *ARG(RARGSZ + 4));
    DISPATCH(RARGSZ + 8);

  }
  //stamp_t2 = GetCPUCount();
  free(callstack);
}



int bc1() {

  void *labels[] = { &&lab1, &&lab2, &&lab3 };

  void *inst[] = { labels[1], labels[0], labels[2] };

  stamp_t1 = GetCPUCount();

  int i;
  register int t =0;



  for (i = 0; i < 10000; i ++) {
    //goto lab3;
    goto *inst[0];

  lab1:
    goto *inst[2];

  lab2:
    goto *inst[1];

  lab3:
    t++;
  }


  stamp_t2 = GetCPUCount();

  return t;

}
