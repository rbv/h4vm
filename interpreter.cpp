#include <stdlib.h>

#include "interpreter.h"
#include "ir_h4.hpp"


void init_interpreter() {
  // Get opcodes pointer
  bcloop(1, NULL, NULL);

  init_data_tables();
}

/********************************************************************/

H4CompiledScript::~H4CompiledScript() {
  if (bytecode)
    free(bytecode);
  bytecode = NULL;
}

bool H4CompiledScript::run(int *locals) {
  bcloop(0, this->bytecode, locals);
  return true;  // TODO
}
