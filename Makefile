CFLAGS = -m32 -g -Wall -Wno-unused-label
CXXFLAGS = -m32 -g -Wall -Wno-unused-function -fno-exceptions -fno-rtti
OBJECTS = main.o loop.o hsz2bc.o datatypes.o ir.o instructions.o emitter.o util.o dbg.o misc.o loadscript.o interpreter.o
BINARY = h4vm_test

all: $(OBJECTS)
#	$(CC) $(CFLAGS) -o $(BINARY) $(OBJECTS)
	fbc -lang deprecated -g $(OBJECTS) -x $(BINARY) -l stdc++

loadscript.o: loadscript.bas
	fbc -lang deprecated loadscript.bas -c -g

test: all
	./$(BINARY) -o test_result.txt && \
	cmp test_result.txt test_correct.txt && \
	rm test_result.txt && \
	echo "All tests passed."

clean:
	rm -f $(OBJECTS) $(BINARY)

s: clean all
