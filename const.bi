
CONST CURRENT_HSZ_VERSION = 3
' .hsz script binary format

CONST CURRENT_HSP_VERSION = 1
' .hs/.hsp file format version
' 0 - HS Header doesn't contain a version number
' 1 - HSpeak 3P

'Constants for scripterr and friends
Enum scriptErrEnum
  serrIgnore = 0     'to suppress an error
  serrInfo = 1       'informative messages
  serrWarn = 2       'possibly suspicious operation, eg. re-freeing a slice
  serrSuspicious = 3 'suspicious operation on weak type or suspicious argument type (unimplemented)
  serrBound = 4      'warning on auto-bound() argument  (suppressed in old games)
  serrBadOp = 5      'bad argument/operation       (not suppressed by default)
  serrError = 6      'corrupt script data/unimplemented feature/interpreter can't continue
  serrBug = 7        'impossible condition; engine bug (interpreter stops)
End Enum


CONST maxScriptGlobals = 16383 'Actually the index of the last global  (also "maximum global id" in plotscr.hsd)
CONST maxScriptStrings = 99 'ID of last plotstring  (also "maximum string id" in plotscr.hsd)
CONST maxScriptHeap = 8192 'Maximum number of local variables in use by all running scripts
CONST maxScriptRunning = 128 'Number of scripts which can run at once
CONST maxScriptNesting = 4 'Maximum subscript nesting depth
#IFDEF SCRIPTPROFILE
'Amount of script data to cache
CONST scriptmemMax = 10000000 'in 4-byte ints
CONST scriptTableSize = 512  'hash table size, power of 2 please
#ELSE
CONST scriptmemMax = 65536 'in 4-byte ints (256kb)
CONST scriptTableSize = 256  'hash table size, power of 2 please
#ENDIF
CONST scriptCheckDelay = 1.1     'How long, in seconds, before the script interpreter becomes interruptable
CONST scriptCheckInterval = 0.1  'How often, in seconds, that the script interpreter should perform checks

CONST maxScriptCmdID = 604  'Max ID number of any supported script command (checked when loading game)

