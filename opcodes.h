#ifndef OPCODES_H
#define OPCODES_H


#ifdef __cplusplus
extern "C" {
#endif

enum op_id {opNIL, opPUSH, opSET_SP, opREF, opDELG, opDEL, opDEL_REGS, opREWRITE_ADDR, opEXIT, opJUMP, opJUMPZ, opJUMPNZ, opFOR_INC_LTEQ_INT, opFOR_INC_GTEQ_INT, opDO, opDO_INT, opLOOP, opINT, opDELG_INT, opMOV_INT, opDOUBLE, opDELG_DOUBLE, opREG, opDELG_REG, opMOV_REG, opSET_REG, opSET_VAR, opENCODE_INT, opENCODE_INT_REG, opDECODE_INT, opADD_INT_INT, opADD_ANY_ANY, opINC_INT_INT, opCAST_DOUBLE, opPRINT_INT, opPRINT_VAR, opPRINT_TABLES, opPRINT_STATE, opSTART_TIMER, opSTOP_TIMER, opCALL_BUILTIN_STACK, opNEW_STRING, };

const char *getopcname(void *op);

#ifdef SWITCH_DISPATCH

#define OPADDR(id)		id

#else //!SWITCH_DISPATCH

#define OPADDR(id)		opcodes[id]

#endif //SWITCH_DISPATCH

/*
enum _op_id {_opNIL, _opPUSH, _opSET_SP, _opREF, _opDELG, _opDEL, _opDEL_REGS, _opREWRITE_ADDR, _opEXIT, _opJUMP, _opJUMPZ, _opJUMPNZ, _opFOR_INC_LTEQ_INT, _opFOR_INC_GTEQ_INT, _opDO, _opDO_INT, _opLOOP, _opINT, _opDELG_INT, _opMOV_INT, _opDOUBLE, _opDELG_DOUBLE, _opREG, _opDELG_REG, _opMOV_REG, _opSET_REG, _opSET_VAR, _opENCODE_INT, _opENCODE_INT_REG, _opDECODE_INT, _opADD_INT_INT, _opADD_ANY_ANY, _opINC_INT_INT, _opCAST_DOUBLE, _opPRINT_INT, _opPRINT_VAR, _opPRINT_TABLES, _opPRINT_STATE, _opSTART_TIMER, _opSTOP_TIMER, _opCALL_BUILTIN_STACK, _opNEW_STRING, };

#define opNIL		OPADDR(_opNIL)
//STARTOPS
#define opPUSH		OPADDR(_opPUSH)
#define opSET_SP		OPADDR(_opSET_SP)
#define opREF		OPADDR(_opREF)
#define opDELG		OPADDR(_opDELG)
#define opDEL		OPADDR(_opDEL)
#define opDEL_REGS		OPADDR(_opDEL_REGS)
#define opREWRITE_ADDR		OPADDR(_opREWRITE_ADDR)
#define opEXIT		OPADDR(_opEXIT)
#define opJUMP		OPADDR(_opJUMP)
#define opJUMPZ		OPADDR(_opJUMPZ)
#define opJUMPNZ		OPADDR(_opJUMPNZ)
#define opFOR_INC_LTEQ_INT		OPADDR(_opFOR_INC_LTEQ_INT)
#define opFOR_INC_GTEQ_INT		OPADDR(_opFOR_INC_GTEQ_INT)
#define opDO		OPADDR(_opDO)
#define opDO_INT		OPADDR(_opDO_INT)
#define opLOOP		OPADDR(_opLOOP)
#define opINT		OPADDR(_opINT)
#define opDELG_INT		OPADDR(_opDELG_INT)
#define opMOV_INT		OPADDR(_opMOV_INT)
#define opDOUBLE		OPADDR(_opDOUBLE)
#define opDELG_DOUBLE		OPADDR(_opDELG_DOUBLE)
#define opREG		OPADDR(_opREG)
#define opDELG_REG		OPADDR(_opDELG_REG)
#define opMOV_REG		OPADDR(_opMOV_REG)
#define opSET_REG		OPADDR(_opSET_REG)
#define opSET_VAR		OPADDR(_opSET_VAR)
#define opENCODE_INT		OPADDR(_opENCODE_INT)
#define opENCODE_INT_REG		OPADDR(_opENCODE_INT_REG)
#define opDECODE_INT		OPADDR(_opDECODE_INT)
#define opADD_INT_INT		OPADDR(_opADD_INT_INT)
#define opADD_ANY_ANY		OPADDR(_opADD_ANY_ANY)
#define opINC_INT_INT		OPADDR(_opINC_INT_INT)
#define opCAST_DOUBLE		OPADDR(_opCAST_DOUBLE)
#define opPRINT_INT		OPADDR(_opPRINT_INT)
#define opPRINT_VAR		OPADDR(_opPRINT_VAR)
#define opPRINT_TABLES		OPADDR(_opPRINT_TABLES)
#define opPRINT_STATE		OPADDR(_opPRINT_STATE)
#define opSTART_TIMER		OPADDR(_opSTART_TIMER)
#define opSTOP_TIMER		OPADDR(_opSTOP_TIMER)
#define opCALL_BUILTIN_STACK		OPADDR(_opCALL_BUILTIN_STACK)
#define opNEW_STRING		OPADDR(_opNEW_STRING)
//ENDOPS
*/

#ifdef WANT_OPDOCS
struct opcode_descript {
  char *name, *args, *docstr;
} opdocs[] = {
  {"NIL", "()", ""},
//STARTOPDOCS
  {"PUSH", "(any $gen)", "push(gen); gen = 0;"},
  {"SET_SP", "(any reg* i)", "stackp = i;"},
  {"REF", "(any $gen)", "ref(gen);"},
  {"DELG", "(any $gen)", "delete(gen); gen = 0;"},
  {"DEL", "(any reg* r)", "delete(*r); *r = 0;"},
  {"DEL_REGS", "(int num)", "for i=0 to num - 1: delete(local[i]);"},
  {"REWRITE_ADDR", "(int val, int num, int offset...)", ""},
  {"EXIT", "()", ""},
  {"JUMP", "(int offset)", "goto offset;"},
  {"JUMPZ", "(int offset, int $gen)", "if (!gen) goto *(pc + offset);"},
  {"JUMPNZ", "(int offset, int $gen)", "if (gen) goto *(pc + offset);"},
  {"FOR_INC_LTEQ_INT", "(int reg* ctr, int reg* step, int reg* end, int repeatjump)", "if (gen = *ctr += *step <= *end) goto repeatjump;"},
  {"FOR_INC_GTEQ_INT", "(int reg* ctr, int reg* step, int reg* end, int repeatjump)", "if (gen = *ctr += *step >= *end) goto repeatjump;"},
  {"DO", "(int reg* looptimes)", ""},
  {"DO_INT", "(int looptimes)", ""},
  {"LOOP", "(void* inst_after_do)", ""},
  {"INT", "(int imm)", "gen = imm;"},
  {"DELG_INT", "(int imm)", "delete(gen); gen = imm;"},
  {"MOV_INT", "(void reg* r, int imm)", "*r = imm;"},
  {"DOUBLE", "(double imm)", "gen = new(imm);"},
  {"DELG_DOUBLE", "(double imm)", "delete(gen); gen = new(imm);"},
  {"REG", "(any reg* r)", "gen = *r;"},
  {"DELG_REG", "(any reg* r)", "delete(gen); gen = *r;"},
  {"MOV_REG", "(void reg* r, any $gen)", "*r = gen;"},
  {"SET_REG", "(any reg* dest, any $gen)", "delete(*dest); *dest = ref(gen);"},
  {"SET_VAR", "(any *dest, any *src)", "delete(*dest); *dest = ref(gen = *src);"},
  {"ENCODE_INT", "(int $gen)", "gen = encode(gen);"},
  {"ENCODE_INT_REG", "(int reg* r)", "*r = encode(*r);"},
  {"DECODE_INT", "(encint *x)", "*x = gen = decode(*x);"},
  {"ADD_INT_INT", "(int reg* inc, int $gen)", "gen += *inc;"},
  {"ADD_ANY_ANY", "(any reg* inc, any $gen)", "gen' = gen + *inc; delete(gen);"},
  {"INC_INT_INT", "(int reg* var, int $gen)", "gen = *var += gen;"},
  {"CAST_DOUBLE", "(int $gen)", "gen = encode((double)gen);"},
  {"PRINT_INT", "(int x)", ""},
  {"PRINT_VAR", "(any reg* r)", ""},
  {"PRINT_TABLES", "()", ""},
  {"PRINT_STATE", "()", ""},
  {"START_TIMER", "()", ""},
  {"STOP_TIMER", "()", ""},
  {"CALL_BUILTIN_STACK", "(short id, short args, any $stack)", "gen = int returnval; $stackp -= args;"},
  {"NEW_STRING", "(var reg* x, const char *c, int len)", "*x = gen := new STRING(c);"},
//ENDOPDOCS
};
#endif //ifdef WANT_OPDOCS

#ifdef __cplusplus
}
#endif

#endif //OPCODES_H
