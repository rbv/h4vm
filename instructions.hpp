
#ifndef INSTRUCTIONS_HPP
#define INSTRUCTIONS_HPP

#include "ir_h4.hpp"

/* IR instructions each have an id number, of type OpCodeTy, and are stored in a
 * an object with type derived from OpCode, or in an instance of OpCode itself,
 * for instructions with no data.

 * int:		Push a literal signed 32 bit int 'i'
 *   IntInstr
 * double:	Push a literal double 'd'
 *   FloatInstr
 * cstr:	Push a literal string 'cstr'
 *   StringInstr
 * reg		Push 'arg'
 *   UnaryInstr
 * setreg:	Copy value into 'arg'. Preserves stack
 *   UnaryInstr
 * pop:		Pop top from stack
 *   OpCode
 * varbegin:	Needs to occur before a temporary variable no. 'reg' enters use.
 *   RegInstr	Could be moved around by optimisation
 * varend:	Marks that a temporary variable no. 'reg' is not used again.
 *   RegInstr	Could be moved around by optimisation. This is optional after a
 *		varbegin.
 * delete:	Delete the contents of a variable 'arg', which might
 *   UnaryInstr	cause side effects. UNIMPLEMENTED. DELETEME?
 * if:		If block, then and else blocks are child OpStreams. Consumes
 *   IfInstr	topmost value on stack to select appropriate block.

 */
enum OpCodeTy {sop_undefined, sop_int, sop_double, sop_cstr, sop_reg, sop_op,
	       sop_pop, sop_setreg, sop_func, sop_if, sop_varbegin, sop_varend,
	       sop_delete};
#ifdef SOP_NAMES
const char *sop_names[] = {"undefined", "int", "double", "cstr", "reg", "op",
			   "pop", "setreg", "func", "if", "varbegin", "varend",
			   "delete"};
#endif

class RegInstr;
class UnaryInstr;
class IntInstr;
class FloatInstr;
class StringInstr;
class OperatorInstr;
class FunctionInstr;
class IfInstr;


/* Generic OpCode derived classes: may be used for several different types */

// Used by sop_pop, sop_if, sop_else, sop_endif
class OpCode : private _H4OpStreamItem {
  friend class H4OpStream;
  friend class H4OpStreamIterator;
public:
  RegInstr &castReg() { return *(RegInstr *)this; }
  UnaryInstr &castUnOp() { return *(UnaryInstr *)this; }
  IntInstr &castInt() { return *(IntInstr *)this; }
  FloatInstr &castFloat() { return *(FloatInstr *)this; }
  StringInstr &castString() { return *(StringInstr *)this; }
  OperatorInstr &castOperator() { return *(OperatorInstr *)this; }
  FunctionInstr &castFunction() { return *(FunctionInstr *)this; }
  IfInstr &castIf() { return *(IfInstr *)this; }

  // Estimate additional memory used by this OpCode
  virtual int calcMemUsage() { return 0; }

  OpCodeTy type;

#ifdef DEBUG_OPSTREAM
  int stacksz_was;
#endif

protected:
  OpCode(OpCodeTy ty) : type(ty) {}
  virtual ~OpCode() {}

  // This form uses the sizeof the object, provided by the compiler in sz,
  // and defaults the alignment to sizeof(int)
  void *operator new(size_t sz, H4OpStream *s) {
    return _H4OpStreamItem::operator new(sz, s, sizeof(int));
  }
  // This form allows the size and alignment to be overridden.
  void *operator new(size_t sz, H4OpStream *s, size_t realsz,
		     size_t alignment = sizeof(int)) {
    return _H4OpStreamItem::operator new(realsz, s, alignment);
  }

private:
  // This declaration would prevent delete from being called on all pointers to
  // classes derived from OpCode. But currently allow new/delete usage.
  //void operator delete(void *, char) {}
};

// Used by sop_varbegin, sop_varend
class RegInstr : public OpCode {
public:
  RegInstr(OpCodeTy ty, int r) : OpCode(ty), reg(r) {}
  int reg;
};

// Used by sop_reg, sop_setreg
class UnaryInstr : public OpCode {
public:
  UnaryInstr(OpCodeTy ty, Operand arg0) : OpCode(ty), arg(arg0) {}
  Operand arg;
};


/* Specific OpCode derived classes: each has fixed type */

class IntInstr : public OpCode {
public:
  IntInstr(int i_) : OpCode(sop_int), i(i_) {}
  int i;
};

class FloatInstr : public OpCode {
public:
  FloatInstr(double d_) : OpCode(sop_double), d(d_) {}
  void *operator new(size_t sz, H4OpStream *s) {
    return OpCode::operator new(sz, s, sizeof(FloatInstr), sizeof(double));
  }
  double d;
};

class StringInstr : public OpCode {
public:
  StringInstr(const char *str_) : OpCode(sop_cstr), str(str_) {}
  void *operator new(size_t sz, H4OpStream *s) {
    return OpCode::operator new(sz, s, sizeof(StringInstr), sizeof(char *));
  }
  const char *str;
};

class OperatorInstr : public OpCode {
public:
  // stackPos is the position in the stack where the argument list starts, going
  // upwards.
  OperatorInstr(IROperator *op_, int stackPos)
    : OpCode(sop_op), op(op_) {
    for (int i = 0; i < op->argc; i++)
      args[i] = Operand(regtyVSTACK, stackPos + i);
  }

  OperatorInstr(IROperator *op_, Operand *arg0, Operand *arg1 = 0,
		Operand *arg2 = 0)
    : OpCode(sop_op), op(op_) {
    args[0] = *arg0;
    if (arg1)
      args[1] = *arg1;
    if (arg2)
      args[2] = *arg2;
  }

  // operator new and the constructor can not share data.
  // Since we need to know the argument count to allocate memory, we need to
  // pass the IROperator to both - urgh
  void *operator new(size_t sz, H4OpStream *s, IROperator *op_) {
    return OpCode::operator new(sz, s,
				sizeof(OperatorInstr) + op_->argc * sizeof(Operand),
				sizeof(void *));
  }

  IROperator *op;
  Operand args[];
};

class FunctionInstr : public OpCode {
public:
  FunctionInstr(int id_, int argc_, int stackPos)
    : OpCode(sop_func), id(id_), argc(argc_) {
    for (int i = 0; i < argc; i++)
      args[i] = Operand(regtyVSTACK, stackPos + i);
  }
  // operator new can not access the constructor arguments, so need to pass argc
  void *operator new(size_t sz, H4OpStream *s, int argc) {
    return OpCode::operator new(sz, s,
				sizeof(FunctionInstr) + argc * sizeof(Operand));
  }
  /*
  Operand getArg(int num) {
    assert(num >= 0 && num < argc);
    if (num <= 2)
      return givenArgs[num];
    fatal("FunctionInstr::getArg: unimplemented");
    //return Operand(regtyVSTACK, );
  }
  */
  int id;
  int argc;
  Operand args[2];
};

class IfInstr : public OpCode {
public:
  IfInstr() : OpCode(sop_if), thenStream(NULL), elseStream(NULL) {}
  ~IfInstr();
  void *operator new(size_t sz, H4OpStream *s) {
    return OpCode::operator new(sz, s, sizeof(IfInstr), sizeof(H4OpStream *));
  }
  int calcMemUsage();

  H4OpStream *thenStream;
  H4OpStream *elseStream;
};

#endif
