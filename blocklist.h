/* SCRAPPED CODE */

/************************* Block doubly-linked lists *************************/

struct _block_dummy {
  void *a, *b;
  char c, d;
};
#define BLOCKDATA (64 - sizeof(struct _block_dummy))
// approx midpoint of the data member, plus compensation for not-quite-full, estimated
//#define MIDPOINT (sizeof(struct _block_dummy) + BLOCKDATA / 2 - 4)

struct bl_block {
  struct bl_block *prev, *next;
  //int nodes;
  /* The number of bytes free in this block (calculated how?)
     Note: the max value of free is BLOCKDATA - 1 */
  unsigned char free;
  /* last is the offset in data[] of the userdata of the last node in this block
     Note: last is not defined on empty blocks (indicated by free == BLOCKDATA) */
  unsigned char last;
  char data[BLOCKDATA];
};
typedef struct bl_block bl_block_t;

struct blocklist {
  //int blocks;
  //int nodes;
  //int blocksize;
  /* Every blocklist contains at least one block. If the bl is empty, it will
   * have one empty block (indicated by .free == BLOCKDATA - 1. Otherwise, blocks
   * are not permitted to be empty! */
  bl_block_t *first, *last;
  //mempool_t *mempool;
};
typedef struct blocklist blocklist_t;


blocklist_t *blocklist_new();
void blocklist_delete(blocklist_t *bl);
void *bl_next(void *ptr);
void *bl_prev(void *ptr);
void *bl_first(blocklist_t *bl);
void *bl_last(blocklist_t *bl);
// Append a node with 'len' bytes
void *bl_append(blocklist_t *bl, int len);
void bl_remove(void *start, int number);
// AKA THISLEN in util.c
#define bl_nodelen(p)	(((unsigned char *)p)[-1] & 15)
