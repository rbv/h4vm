#include "instructions.hpp"

using namespace std;

IfInstr::~IfInstr() {
  delete thenStream;
  delete elseStream;
}

int IfInstr::calcMemUsage() {
  return (thenStream ? thenStream->calcMemUsage() : 0) +
         (elseStream ? elseStream->calcMemUsage() : 0);
}
