#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "emit.hpp"
#include "emitter_private.hpp"
#include "opcodes.h"

using namespace std;

void **opcodes;

//int VmModel::Dbg::count = 0;

/*
 * future (unimplemented) ideas for the STACK
 * If 1 byte register number args are used, need to conserve registers a bit.
 * Also, since call nesting is unbounded, top of stack might go > 256
 * Therefore, only push things on the stack for opcodes that read from the
 * top of the stack, and never manipulate it by outputting register numbers on the stack.
 * Add an opcode to free things by stack position.
*/


void VarContent::unionWith(const VarContent &rhs) {
  if (is(vbitREFC) != rhs.is(vbitREFC))
    fatal("Attempted union between refc'd and non-refc'd values"); //TODO
  if (isFree() != rhs.isFree())
    fatal("Attempted union between free and non-free values:\n%s%s", toString().c_str(), rhs.toString().c_str());

  if (type() != rhs.type()) { 
    if (isNull() || rhs.isNull()) {
      fatal("Attempted union between null and non-null values:\n%s%s", toString().c_str(), rhs.toString().c_str());
    }

    initWith(vctANY);
  } else {
    if (is(vbitMAYBETYPED | vbitTYPED) != rhs.is(vbitMAYBETYPED | vbitTYPED))
      markMaybeTyped();

    if (is(vbitDECODED | vbitMAYBEENC | vbitENCODED) != rhs.is(vbitDECODED | vbitMAYBEENC | vbitENCODED))
      markMaybeEnc();

    addBits(rhs.is(vbitMODIFIED));
  }
}

string VarContent::toString() const {
  static const char *typenames[] = {"null", "any", "int", "string", "double", "array", "udt"};
  static char buf[128];
  sprintf(buf, "%8x: %-7s %s%s%s%s%s%s%s%s%s\n", bits, typenames[type()],
          (is(vbitFREE) ? " FREE" : ""),
          (is(vbitREFC) ? " REFC" : ""),
          (is(vbitMAYBETYPED) ? " MAYBETYPED" : ""),
          (is(vbitTYPED) ? " TYPED" : ""),
          (is(vbitDECODED) ? " DECODED" : ""),
          (is(vbitMAYBEENC) ? " MAYBEENC" : ""),
          (is(vbitENCODED) ? " ENCODED" : ""),
          (is(vbitNUMBER) ? " NUMBER" : ""),
          (is(vbitMODIFIED) ? " MODIFIED" : ""));
  return string(buf);
}

VmModel::VmModel(int args, int locals, int temporaries) {
  stacksz = 0;

  regs.resize(locals);
  int i;
  for (i = 0; i < args; i++)
    regs[i] = vctSML_INT;  // TODO
  for (; i < locals; i++)
    regs[i] = vctSML_INT;  // 0, in fact

  gentype = vctFREE;

  vtemplist.resize(temporaries, Operand(regtyNONE));
}

void VmModel::print() const {
  unsigned int i;
  printf("<<<Model :\n");
  printf("    vstack:\n");
  for (i = 0; i < vstack.size(); i++)
    printf("        %s\n", vstack[i].toString().c_str());
  printf("    vtemplist:\n");
  for (i = 0; i < vtemplist.size(); i++)
    printf("        %s\n", vtemplist[i].toString().c_str());

  printf("    gen:\n");
  printf("      %s", gentype.toString().c_str());
  printf("    regs:\n");
  for (i = 0; i < regs.size(); i++)
    printf("      %s", regs[i].toString().c_str());

  printf("    stk:\n");
  for (i = 0; i < stk.size(); i++) {
    if (i == stacksz)
      printf("      -stacktop = %d -\n", stacksz);
    printf("      %s", stk[i].toString().c_str());
  }
  if (i == stacksz)
    printf("      -stacktop = %d -\n", stacksz);
  printf("    >>>\n");
}

EmitState::EmitState(int args, int locals, H4OpStream *stream) {
  codealloc = 10240;
  p = code = (char *)malloc(codealloc);
  argc = args;
  localc = locals;
  state = new VmModel(args, locals, stream->numTemps());
}

EmitState::~EmitState() {
  delete state;

  printf("~EmitState()\n");

  if (code) {
    printf("EmitState: ownership of code was not transferred (.emit() not called)");
    free(code);
    code = p = NULL;
    codealloc = 0;
  }
  printf("~EmitState()\n");

}

H4CompiledScript *EmitState::emitCode(H4OpStream *stream) {
  /*
  // Register remapping phase
  processStream(stream, false);
  */
  // Emit phase
  PUSH_OP(p, opSET_SP);
  PUSH_STKREG(p, this, 0);
  processStream(stream, true);
  PUSH_OP(p, opPRINT_STATE);
  PUSH_OP(p, opEXIT);

  printf("\nfinishing script - emitted %d bytes bytecode\n" "max regs = %d, max stack = %d, %d stack reg rewrites\n",
         (int)(p - code), (int)state->regs.size(), (int)state->stk.size(), (int)stk_reg_addrs.size());

  unsigned int i;
  for (i = 0; i < stk_reg_addrs.size(); i++)
    *(LOCAL_NUM_TYPE *)(code + stk_reg_addrs[i]) += state->regs.size();

  //initialsp = state->regs.size() - 1;

  // Transfer bytecode ownership to a CompiledScript object
  H4CompiledScript *ret = new H4CompiledScript((void**)code);
  p = code = NULL;
  codealloc = 0;
  return ret;
}

Operand &VmModel::decodeArg(Operand &operand) {
  switch (operand.type()) {
  case regtyREAL:
    assert(operand.num() >= 0 && operand.num() < (int)regs.size());
    return operand;
  case regtySTACK:
    assert(operand.num() >= 0 && operand.num() < (int)stk.size());
    // fall
  case regtyGEN:
    return operand;
  case regtyVSTACK:
    assert(operand.num() >= 0 && operand.num() < (int)vstack.size());
    assert(vstack[operand.num()].type() != regtyVSTACK);
    // TODO OPTIMISATION: If carefully dereference early, we could make sure it could not be a VTEMP
    return decodeArg(vstack[operand.num()]);
  case regtyVTEMP:
    assert(operand.num() >= 0 && operand.num() < (int)vtemplist.size());
    assert(vtemplist[operand.num()].type() != regtyVSTACK);
    // TODO OPTIMISATION: If carefully dereference early, we could make sure it could not be a VTEMP
    return decodeArg(vtemplist[operand.num()]);
  default:
    fatal("bad reg type %d", operand.type());
  }
}

VarContent &VmModel::getReg(const Operand &operand) {
  switch (operand.type()) {
  case regtyGEN:
    return gentype;
  case regtyREAL:
    assert(operand.num() >= 0 && operand.num() < (int)regs.size());
    return regs[operand.num()];
  case regtySTACK:
    assert(operand.num() >= 0 && operand.num() < (int)stk.size());
    return stk[operand.num()];
  case regtyVSTACK:
    assert(operand.num() >= 0 && operand.num() < (int)vstack.size());
    assert(vstack[operand.num()].type() != regtyVSTACK);
    return getReg(vstack[operand.num()]);
  case regtyVTEMP:
    assert(operand.num() >= 0 && operand.num() < (int)vtemplist.size());
    assert(vtemplist[operand.num()].type() != regtyVSTACK);
    return getReg(vtemplist[operand.num()]);
  default:
    fatal("bad reg type %d", operand.type());
  }
}

void VmModel::assertUnifiable(const VmModel &rhs) const {
  if (stacksz != rhs.stacksz)
    fatal("H4OpStream: different flows through script leaving stack different sizes"); // bad opstream
  if (vstack.size() != rhs.vstack.size())
    fatal("H4OpStream: different flows through script leaving virtual stack different sizes"); // emitter bug
  if (regs.size() != rhs.regs.size())
    fatal("H4OpStream: different flows through script using different number of registers"); // TODO/emitter bug
  // vtemplist size difference currently impossible

  unsigned int i;
  for (i = 0; i < vstack.size(); i++)
    if (vstack[i] != rhs.vstack[i])
      fatal("H4OpStream: different flows through script remapping virtual stack differently"); // TODO/emitter bug
  for (i = 0; i < vtemplist.size(); i++)
    if (vtemplist[i] != rhs.vtemplist[i])
      fatal("H4OpStream: different flows through script remapping virtual temporaries differently"); // TODO/emitter bug
}

void VmModel::unionWith(const VmModel &rhs) {
  assertUnifiable(rhs);

  gentype.unionWith(rhs.gentype);

  unsigned int i;
  for (i = 0; i < regs.size(); i++)
    regs[i].unionWith(rhs.regs[i]);
  for (i = 0; i < stacksz; i++)
    stk[i].unionWith(rhs.stk[i]);
  for (i = stacksz; i < stk.size(); i++) {
    if (stk[i].is(vbitREFC)) {
      print();
      fatal("refcounted object alive past end of stack");
    }
    stk[i] = vctFREE;
  }
  for (i = stacksz; i < rhs.stk.size(); i++) {
    if (rhs.stk[i].is(vbitREFC)) {
      print();
      fatal("refcounted object alive past end of stack");
    }
  }
}

bool VmModel::typesEqual(const VmModel &rhs) const {
  assertUnifiable(rhs);

  if (gentype.type() != rhs.gentype.type())
    return false;

  unsigned int i;
  for (i = 0; i < regs.size(); i++)
    if (regs[i].type() != rhs.regs[i].type())
      return false;
  for (i = 0; i < stk.size(); i++)
    if (stk[i].type() != rhs.stk[i].type())
      return false;

  return true;
}

void VmModel::clearModifiedFlags() {
  unsigned int i;
  for (i = 0; i < regs.size(); i++)
    regs[i].unmodify();
  for (i = 0; i < stk.size(); i++)
    stk[i].unmodify();
  gentype.unmodify();
}

// Just delete the contents of gen, if necessary, and mark unused
void VmModel::emptyGen(EmitState *st) {
  // Should consider anywhere other than end of vstack illegal
  if (!vstack.empty() && vstack.back().type() == regtyGEN) {
    assert(!gentype.isFree() && !gentype.isNull());
    vstack.pop_back();
  }

  if (!gentype.isFree()) {
    if (gentype.is(vbitREFC)) {  // | vbitMAYBEENC ?
      //gen is going to get clobbered, it's up to you to save it!
      if (st) {
	PUSH_OP(st->p, opDELG);
      }
    }
  } else
    printf("\nwarning: emptying unused gen\n");
  gentype = vctFREE;
}

// Could have an option to not set gen to vctFREE?
void VmModel::pushGen(EmitState *st) {
  // Should consider anywhere other than end of vstack illegal
  if (!vstack.empty() && vstack.back().type() == regtyGEN) {
    assert(!gentype.isFree() && !gentype.isNull());
    vstack.back() = Operand(regtySTACK, stacksz);
  }

  //what's this gen doing hanging around here? It's wanted on the stack!
  if (!gentype.isFree()) {
    if (gentype.isNull())
      fatal("tried to push null gen");

    if (st) {
      PUSH_OP(st->p, opPUSH);
    }
    //stk.push_back(gentype);
    stacksz++;
    if (stk.size() < stacksz)
      stk.resize(stacksz);
    stk[stacksz - 1] = gentype;
  } /* else
       printf("\nwarning:tried to push unused gen\n"); */
  gentype = vctFREE;
}

void VmModel::moveToGen(const Operand &operand, EmitState *st) {
  if (getRegType(operand) != regtyGEN) {
    if (!gentype.isFree()) {
      print();
      fatal("gen not available!");
    }
    fatal("moveToGen not implemented");
  }
}

void EmitState::emitOperand(const Operand &operand) {
  Operand realArg = state->decodeArg(operand);
  if (realArg.type() == regtyREAL) {
    PUSH_REG(p, realArg.num());
  } else if (realArg.type() == regtySTACK) {
    PUSH_STKREG(p, this, realArg.num());
  } else
    fatal("emitting bad operand (real type %d):\n%s", realArg.type(), realArg.toString().c_str());
}

/*
//not yet used
static void cons_prop(EmitState *st, OpCode *icode) {
  if (icode->type == sop_int) {
    int r = emitter_makeconst_int(st, icode->i);
    if (r) {
      icode->type = sop_reg;
      icode->reg = r;
    }
  }
}

//returns 0 if not created
int emitter_makeconst_int(EmitState *st, int val) {
  return 0;
}
*/

//returns a register number
Operand VmModel::allocTempVariable() {
  unsigned int i;
  for (i = 0; i < regs.size(); i++) {
    if (regs[i].is(vbitFREE)) {
      regs[i] = vctNULL;
      DPRINT(("\nalloced reg %d as temp\n", i));
      return Operand(regtyREAL, i);
    }
  }
  regs.push_back(VarContent(vctNULL));
  DPRINT(("\ncreated new reg %d as temp\n", (int)regs.size() - 1));
  return Operand(regtyREAL, regs.size() - 1);
}

void VmModel::deleteTempVariable(Operand which) {
  assert(which.type() == regtyREAL);
  VarContent reg = getReg(which);
  assert(!reg.is(vbitREFC) && !reg.isFree());
  reg = vctFREE;
}

//returnval is true if this call should produce a return value,
//otherwise the return value is discarded
void VmModel::emitCall(FunctionInstr *ip, EmitState *st) {
  int i;
  // TODO: move things into place or alternative VM op; for now we just require them already positioned
  pushGen(st);
  for (i = 0; i < ip->argc; i++) {
    Operand realArg = decodeArg(ip->args[i]);
    if (realArg.type() != regtySTACK || realArg.num() != (int)stacksz - ip->argc + i) {
      print();
      fatal("Expected function's args to be laid out on stack at offset %d", stacksz - ip->argc);
    }
  }

  if (st) {
    PUSH_OP(st->p, opCALL_BUILTIN_STACK);
    PUSH_SHORT(st->p, ip->id);
    PUSH_SHORT(st->p, ip->argc);
  }
  // Op automatically decrements stackp
  stacksz -= ip->argc;

  //clean thy shit up
  for (i = 0; i < ip->argc; i++) {
    if (stk[stacksz + i].is(vbitREFC)) { // | vbitMAYBEENC ?
      if (st) {
        PUSH_OP(st->p, opDEL);
	st->emitOperand(ip->args[i]);
	//PUSH_STKREG(st->p, st, stacksz + i);
      }
    }
    stk[stacksz + i] = vctFREE;
  }
  vstack.resize(vstack.size() - ip->argc);

  // Assume value returned, for now - TODO
  gentype = vctPLAIN_INT;
  vstack.push_back(Operand(regtyGEN));
}

void VmModel::emitOp(OperatorInstr *ip, EmitState *st) {
  IROperator *op = ip[0].op;

  if(op->argc == 0)
    fatal("TODO: Can't handle nulary ops");

  // Last arg needs to be in gen
  Operand &lastarg = ip->args[op->argc - 1];
  moveToGen(lastarg, st);

  //check what kind of args it's been passed
  //TODO: generalise this to non-integer stuff
  bool have_non_plainint = false;
  int i;
  for (i = 0; i < op->argc; i++) {
    VarContent &var = getReg(ip->args[i]);
    if (var.type() != vtINT || var.needDecode())
      have_non_plainint = true;
  }

  vccompound_t retType;

  if (!have_non_plainint && op->op_int) {
    //great; there's a version of the IROperator which takes and returns unencoded ints
    if (st) {
      PUSH_OP(st->p, op->op_int);
    }
    retType = vctPLAIN_INT;
  } else {
    //too bad; use most general version
    //have to encode any ints
    for (i = 0; i < op->argc - 1; i++) {
      VarContent &var = getReg(ip->args[i]);
      if (var.type() == vtINT && var.needEncode()) {
	if (st) {
	  if (getRegType(ip->args[i]) == regtyGEN) {
	    PUSH_OP(st->p, opENCODE_INT);
	  } else {
	    PUSH_OP(st->p, opENCODE_INT_REG);
	    st->emitOperand(ip->args[i]);
	    //PUSH_STKREG(st->p, st, stacksz - i);
	  }
	}
	var = vctENC_INT;
      }
    }
    if (st) {
      PUSH_OP(st->p, op->op_any);
    }
    retType = vctANY;
  }

  if (st) {
    for (i = 0; i < op->argc - 1; i++) {
      st->emitOperand(ip->args[i]);
    }
  }
  //since generally registers are not passed on the stack, most ops don't
  //decrement stackp. TODO: generalise and remove
  if (op->argc > 1) {
    stacksz -= op->argc - 1;
    if (st) {
      PUSH_OP(st->p, opSET_SP);
      PUSH_STKREG(st->p, st, stacksz);
    }
  }
  vstack.resize(vstack.size() - op->argc);

  gentype = retType;
  vstack.push_back(Operand(regtyGEN));
}

void EmitState::branchIf(IfInstr &ifIn, bool emit) {
  // FIXME: use moveToGen
  if (state->gentype.type() != vtINT) { // FIXME!! purposefully wrong: opJUMPZ requires DECODED INT
    state->print();
    fatal("H4OpStream:if must operate on integral type");
  }
  state->gentype = vctFREE; //DELETEME?
  state->vstack.pop_back();

  if (!ifIn.thenStream && !ifIn.elseStream) {
    printf("Warning: if block without then or else\n");
    // Since conditional already emitted, and we assume conditional result is
    // decoded (not REFC), we can delete the rest of the if block.
    return;
  }

  bool invert = false;  // Swap then and else
  if (!ifIn.thenStream) {
    ifIn.thenStream = ifIn.elseStream;
    ifIn.elseStream = NULL;
    invert = true;
  }

  int *elseJump;     // Location in bytecode of jump to else/past then block.
  int *endThenJump;  // Jump past else block.

  if (emit) {
    if (!invert) {
      PUSH_OP(p, opJUMPZ);
    } else {
      PUSH_OP(p, opJUMPNZ);
    }
    elseJump = (int *)p;
    PUSH_INT(p, 0);  // placeholder
  }

  VmModel *startState = state;
  VmModel *thenState = NULL;  // State at end of then block

  state = new VmModel(*state);

  processStream(ifIn.thenStream, emit);
  state->pushGen(emit ? this : NULL);

  if (ifIn.elseStream) { // Both then() and else()
    // Preserve state and rewind
    thenState = state;
    state = startState;
    startState = NULL;

    if (emit) {
      PUSH_OP(p, opJUMP);
      endThenJump = (int *)p;
      PUSH_INT(p, 0);  // Placeholder

      *elseJump = p - code;
    }

    processStream(ifIn.elseStream, emit);
    state->pushGen(emit ? this : NULL);

    // Point the jump at end of then() past if-block
    if (emit)
      *endThenJump = (int)(p - code);

    DPRINT(("\nUNION then(), else():\n"));
    DEBUG(thenState->print());
    DEBUG(state->print());

    state->unionWith(*thenState);
    delete thenState;

  } else { // Just then()
    // Point the condition-false (else) jump past if-block
    if (emit)
      *elseJump = (int)(p - code);

    DPRINT(("\nUNION original, then():\n"));
    DEBUG(startState->print());
    DEBUG(state->print());

    state->unionWith(*startState);
    delete startState;
  }
  DPRINT(("RESULT:\n"));
  DEBUG(state->print());
}

H4OpStream::iterator VmModel::processStream(H4OpStream *opStream, H4OpStream::iterator ip, EmitState *st) {
  while (ip != opStream->end()) {
    //printf (" emitting type %d\n", ip->type);
    switch (ip->type) {
    case sop_int:
      pushGen(st);
      vstack.push_back(Operand(regtyGEN));
      gentype = vctPLAIN_INT;
      if (st) {
        PUSH_OP(st->p, opINT);
        PUSH_INT(st->p, ip->castInt().i);
      }
      break;
    case sop_double:
      pushGen(st);
      vstack.push_back(Operand(regtyGEN));
      gentype = vctENC_DOUBLE; // | vbitREFC;
      if (st) {
        PUSH_OP(st->p, opDOUBLE);
        PUSH_DOUBLE(st->p, ip->castFloat().d);
      }
      break;
    case sop_reg:
      // TODO: OPTIMISATION: Don't put in gen, just update vstack! (That's the whole point)
      {
	Operand &arg = ip->castUnOp().arg;
	pushGen(st);
	vstack.push_back(Operand(regtyGEN));
	gentype = getReg(arg); // | vbitREFC;
	if (st) {
	  PUSH_OP(st->p, opREG);
	  st->emitOperand(arg);
	}
      }
      break;
    case sop_setreg:
      // Leave gen intact
      {
	Operand realArg = decodeArg(ip->castUnOp().arg);
	if (st) {
	  if (getReg(realArg).is(vbitREFC)) {
	    // delete(dest) and ref(src)
	    PUSH_OP(st->p, opSET_REG);
	    st->emitOperand(realArg);
	  } else {
	    // plain move
	    PUSH_OP(st->p, opMOV_REG);
	    st->emitOperand(realArg);
	    if (gentype.is(vbitREFC)) {
	      PUSH_OP(st->p, opREF);
	    }
	  }
	}
	getReg(realArg) = gentype;
      }
      break;
    case sop_varbegin:
      {
	int reg = ip->castReg().reg;
	assert(reg >= 0 && reg < (int)vtemplist.size());
	assert(vtemplist[reg].type() == regtyNONE);
	vtemplist[reg] = allocTempVariable();
      }
      break;
    case sop_varend:
      {
	int reg = ip->castReg().reg;
	assert(reg >= 0 && reg < (int)vtemplist.size());
	deleteTempVariable(vtemplist[reg]);
	vtemplist[reg].set(regtyNONE);
      }
      break;
    case sop_op:
      emitOp(&ip->castOperator(), st);
      break;
    case sop_func:
      emitCall(&ip->castFunction(), st);
      break;
    case sop_pop:
      emptyGen(st);
      break;
    case sop_if:
      return ip;
    default:
      fatal("bad stack inst type %d", ip->type);
    }
    //print();
    ++ip;
  }
  return ip;
}

void EmitState::processStream(H4OpStream *opStream, bool emit) {
  H4OpStream::iterator ip = opStream->begin();
  while (ip != opStream->end()) {
    //printf (" emitting type %d\n", ip->type);
    switch (ip->type) {
    case sop_if:
      branchIf(ip->castIf(), emit);
      break;
    default:
      ip = state->processStream(opStream, ip, emit ? this : NULL);
      continue;
    }
    ++ip;
  }
}
