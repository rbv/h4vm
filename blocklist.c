/* SCRAPPED CODE */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
// for debugging
#include <stdio.h>

#include "util.h"
#include "interpreter.h"
#include "blocklist.h"

/************************* Block doubly-linked lists *************************/

#define GETBLK(p)	((bl_block_t *)((int)(p) & ~63))
#define THISLEN(p)	(((unsigned char *)p)[-1] & 15)
#define PREVLEN(p)	(((unsigned char *)p)[-1] / 16)
#define MAKELEN(prevl, thisl)	(unsigned char)(prevl * 16 + thisl)
// from node pointer inside block to .data[] index
#define GETOFF(p)	(((int)(p) & 63) - sizeof(struct _block_dummy))

mempool_t *bl_mempool;

/* Allocate and construct a new block, insert it into bl after afterwhat, or at
 * beginning if NULL. .last member is left uninitialised
 * Note: .free is set to BLOCKDATA - 1 even though .data[0] is not set!
 */
static bl_block_t *bl_newblknext(blocklist_t *bl, bl_block_t *afterwhat) {
  bl_block_t *blk;
  //blk = malloc(sizeof(bl_block_t));
  blk = mempool_allocblk(bl_mempool);
  assert(GETBLK(blk) == blk);
  //FIXME: bl->blocks++;
  if (afterwhat) {
    blk->next = afterwhat->next;
    if (afterwhat->next)
      //not last in blocklist
      afterwhat->next->prev = blk;
    blk->prev = afterwhat;
    afterwhat->next = blk;
  } else {
    blk->prev = NULL;
    blk->next = bl->first;
    if (bl->first)
      //not a new blocklist
      bl->first->prev = blk;
    bl->first = blk;
  }

  blk->free = BLOCKDATA - 1;
  //don't set last as empty block not valid
  return blk;
}

/* Returns next block */
static bl_block_t *bl_freeblk(bl_block_t *blk) {
  if (blk->prev)
    blk->prev->next = blk->next;
  if (blk->next)
    blk->next->prev = blk->prev;
  bl_block_t *temp = blk->next;
  mempool_freeblk(bl_mempool, blk);
  //FIXME: bl->blocks--
  return temp;
}

blocklist_t *blocklist_new() {
  if (!bl_mempool)
    bl_mempool = mempool_new(sizeof(bl_block_t));

  blocklist_t *bl;
  bl = malloc(sizeof(blocklist_t));
  //bl->blocksize = 256;
  //bl->blocks = 0;
  //bl->nodes = 0;

  bl->first = bl->last = NULL;
  // bl_newblknext will set just bl->first
  bl->last = bl_newblknext(bl, NULL);

  return bl;
}

void blocklist_delete(blocklist_t *bl) {
  bl_block_t *blk = bl->first;
  while (blk)
    blk = bl_freeblk(blk);
  free(bl);
}

void *bl_next(void *ptr) {
  ptr += THISLEN(ptr);
  if(((char *)ptr)[-1])
    return ptr;

  //off the end of the block
  bl_block_t *b = GETBLK(ptr - 1)->next;
  if (!b)
    {
      printf("  end ");
    return NULL;
    }
  return b->data + 1;
}

void *bl_prev(void *ptr) {
  if (!PREVLEN(ptr)) {
    //off the end of the block
    bl_block_t *b = GETBLK(ptr)->prev;
    if (!b)
      return 0;
    return &b->data[b->last];
  }

  return ptr - PREVLEN(ptr);
}

/* First item in a blocklist, or NULL if it is empty */
void *bl_first(blocklist_t *bl) {
  if (bl->first->free == BLOCKDATA - 1)
    return NULL;
  return bl->first->data + 1;
}

/* Last item in a blocklist, or NULL if it is empty */
void *bl_last(blocklist_t *bl) {
  if (bl->first->free == BLOCKDATA - 1)
    return NULL;
  return bl->last->data + bl->last->last;
}

/* number of nodes in the bl
int bl_size(blocklikst_t *bl) {
  int size;
}
*/

/* returns the i-th item in the blocklist, or NULL if it goes over the end */
void *bl_index(blocklist_t *bl) {

}

void *bl_append(blocklist_t *bl, int len) {
  //note: bl could be empty, so b would be empty
  assert(len >= 1 && len <= 14);
  bl_block_t *b = bl->last;
  char *result;
  int oldlastlen = 0;
  if (b->free < len + 1) {
    b = bl_newblknext(bl, b);
    b->last = 1;
    result = b->data + 1;
  } else {
    if (b->free != BLOCKDATA - 1) {
      result = b->data + b->last;
      oldlastlen = THISLEN(result);
      b->last += oldlastlen;
      result += oldlastlen;
    } else {
      b->last = 1;
      result = b->data + 1;
    }
  }
  result[-1] = MAKELEN(oldlastlen, len + 1);
  result[len] = 0;

  b->free -= len + 1;
  return result;
}

/* Deletes a splice specified by start and number of nodes */
void bl_remove(void *start, int number) {
  assert(number >= 1);

  // ptr is the current node, start is the first node in this block visited
  char *ptr = start;
  bl_block_t *b = GETBLK(start);
  // whether we started this block at its first node
  int startedblock = (start == b->data + 1);

  //FIXME: nodes--
  for (; number > 0; number--) {
    ptr += THISLEN(ptr);

    if (!ptr[-1]) {
      //reached end of block, clean it up
      if (!startedblock) {
	//started midway
	b->last = GETOFF(start) - PREVLEN(start);
	b->free = BLOCKDATA - GETOFF(start);
	((char *)start)[-1] = 0;
	//FIXME: blocks--
	b = b->next;
      } else {
	//delete the block
	b = bl_freeblk(b);
      }
      if (!b || number == 1)
	return;
      ptr = b->data + 1;
      start = ptr;
      startedblock = 1;
    }
  }
  int removed = ptr - (char *)start;
  // set the length byte now instead of overwriting it
  ((char *)start)[-1] = MAKELEN(PREVLEN(start), THISLEN(ptr));
  // move the end of the block down  >>>TODO: check maths
  memmove(start, ptr, (BLOCKDATA - b->free) - GETOFF(ptr));
  b->free += removed;
  b->last -= removed;
}

static void bl_makeroom(blocklist_t *bl, char *start, int len) {


}

// Move as many nodes as possible from the start of one block to the end of another,
// but don't modify the from block. Cleans up the to block.
// Don't move the node at data[max]. Return number of bytes moved
static int bl_move_to_end(bl_block_t *fromb, bl_block_t *tob, int max) {
  char *dest = tob->data + tob->last;
  int prevlen = THISLEN(dest);
  dest += prevlen;

  char *src = fromb->data + 1;

  int moved = 0;

  while (1) {
    int piecelen = THISLEN(src);
    if (piecelen < tob->free)
      break;
    if (moved + piecelen > max)
      break;
    dest[-1] = MAKELEN(prevlen, piecelen + 1);

    for (int i = 0; i < piecelen - 1; i++)
      dest[i] = src[i];

    moved += piecelen;
    src += piecelen;
    dest += piecelen;
    prevlen = piecelen;
  }
  dest[-1] = MAKELEN(prevlen, 0);
  return moved;
}


// insert a single node before start. NOTE: start won't be valid afterwards
void bl_insert(blocklist_t *bl, void *start, int len) {
  assert(len >= 1 && len <= 14);
  bl_block_t *b = GETBLK(start);
  char *result;
  int oldlastlen = 0;
  if (b->free < len + 1) {
    if (GETOFF(start) < b->free / 2) {
    //if (start - (void *)b < MIDPOINT) {
      // look back a block
      bl_block_t *backb = b->prev;
      if (backb->free > 8) { // a heuristic
	int freed = bl_move_to_end(b, backb, GETOFF(start)); //CONDITIONAL
	if (freed && GETOFF(start) - freed) {
	  // move everything upto start down, not including the last length token!
          memmove(b->data, b->data + freed, GETOFF(start) - freed);
          b->data[0] = MAKELEN(0, THISLEN(b->data[0]));
	}
	if (b->free + freed < len + 1) {
	  // damn! Split anyway
	  // amount of stuff after the inserting point: where to put it?
	  int splitsize = (BLOCKDATA - b->free) - GETOFF(start);
	  int firstlen;
	  if (!multiinsert) {
	    // don't be precious about positioning the new node: it can go with the rest
	    splitsize += len + 1;
	    firstlen = len + 1;
	  } else
	    firstlen = THISLEN(start);
	  if (b->next && splitsize <= b->next->free) {
	    // shunt to next block
	    bl_move_to_beginning(b, b->next, start); //NOT CONDITIONAL, JUST MOVE ALL
	    //... TODO
	  } else {
	    // ok, to new block!
	    bl_block_t *newb = bl_newblknext(bl, b);
	    memcpy(newb->data + 1, start, splitsize);
	    newb->data[0] = MAKLEN(0, firstlen);
            //TODO
	    newb->free -= splitsize;
	    //newb->last = ;
	  }
	  if (multiinsert) {
	    // insert the new node into its own block!
	    bl_block_t *newb = bl_newblknext(bl, b);
	    newb->data[0] = MAKELEN(0, len + 1);
	    newb->data[len + 1] = 0;
	    newb->free -= len + 1;
	    newb->last = 1;
	  }
	} else {
	  char *insertpos = &b->data[GETOFF(start)] - freed;
	  char followingtok = MAKELEN(len + 1, THISLEN(start));
	  insertpos[-1] = MAKELEN(PREVLEN(start), len + 1);
	  // move end of block, not including preceeding token!, to right position
	  memmove(insertpos + len + 1, start + THISLEN(start), (BLOCKDATA - b->free) - (GETOFF(start) + len + 1));
	  insertpos[len] = followingtok;
	  b->free += freed - (len + 1);
	  return insertpos;
	}

	b->free += freed;


      }

    }
    b = bl_newblknext(bl, b);
    b->last = 1;
    result = b->data + b->last;
  } else {

  }
  result[-1] = MAKELEN(oldlastlen, len + 1);
  result[len] = 0;

  b->free -= len + 1;
  //ought to return after insertion
  //return result;
}

/* Splice one blocklist into another (deleting it).
 * This is meant to be a constant time operation for linked lists; balance
 * time and space efficiency. Follow this algorithm:
 * -If the blocklist to be spliced in contains just one block, try and fit it
 *  in:
 *  -See if it can be inserted into the destination block
 *  -Otherwise see if some nodes can be moved to one of the previous or next
 *   blocks
 *  -Otherwise split it
 */
void bl_splice(blocklist_t *bl, void *start, blocklist_t *insert) {

}

/* Check the integrity of a blocklist */
void bl_check(blocklist_t *bl) {
  assert(bl->first != NULL && bl->last != NULL);
  // check for empty blocklists
  //  if (bl->first == bl->next) {
  //if

  int nodes = 0;

  bl_block_t *blk = bl->first;
  bl_block_t *prev_blk = NULL;

  while (blk) {
    assert(GETBLK(blk) == blk);
    // Not checked: whether blk is a real mempool block
    assert(blk->prev == prev_blk);
    if (blk->free == BLOCKDATA - 1) {
      // only valid for empty blocklists
      assert(blk == bl->first);
      assert(blk == bl->last);
      assert(blk->next == NULL);
      printf("\nBlocklist empty.\n");
      return;
    }

    assert(blk->free < BLOCKDATA - 1 && blk->free >= 0);

    char *node = blk->data + 1;
    char *prev_node = NULL;

    while (node[-1]) {
      assert(THISLEN(node) >= 1);
      if (prev_node) {
	assert(PREVLEN(node) == THISLEN(prev_node));
      } else {
	assert(PREVLEN(node) == 0);
      }
      nodes++;
      prev_node = node;
      node += THISLEN(node);
      // allowed to go 1 byte over the end
      assert(node <= (void *)(blk + 1));
    }
    assert(GETOFF(prev_node) == blk->last);
    assert(BLOCKDATA - GETOFF(node) == blk->free);

    prev_blk = blk;
    blk = blk->next;
  }
  assert(prev_blk == bl->last);

  printf("\nBlocklist contained %d nodes.\n", nodes);
}
