
#ifndef IR_HPP
#define IR_HPP


// A compiled script
class CompiledScript {
public:
  virtual ~CompiledScript() {};

  // (Placeholder only) True if no error occurred
  virtual bool run(int *locals) = 0;
};


// These operator numbers are used only by the OpStream interface.
// Not to be confused with VM opcodes and ids, eg opADD and _opADD, or IR op descriptors, eg opdAdd
// NOTE: must update irOpTable in ir_h4.cpp and irOpTable in ir_luatext.cpp when changing this!
enum OperatorNum {
  opnumInvalid = 0,
  opnumAdd,
  opnumSub,
  opnumMult,
  opnumDivInt,
  opnumDivFloat,
  opnumMod,
  opnumEqual,
  opnumInequal,
  opnumNUM
};

// A register number.
// Can refer to a local, arg, upval, or temporary local.
// Values 0 <= x < args+locals+upvals refer to locals and arguments
// Others are temporary local variables created by calling newTemp().
typedef int VirtualRegNum;

// OpStream implements the BaseOpStream interface. Use OpStream instead.
// OpStream will be typedef'ed to one of the OpStream implementations:
// -H4OpStream: target the H4VM
// -LuaTextOpStream: output textual lua scripts, portable to any lua 5.1 interpreter

//class OpStream;

class BaseOpStream {
public:

  // Set number of arguments and locals
  BaseOpStream() {}
  BaseOpStream(int id, int args, int locals, int upvals) {}
  ~BaseOpStream() {}

  CompiledScript *emit();

  // Return the register number for the i-th...
  VirtualRegNum localNum(int which);
  VirtualRegNum argNum(int which);
  VirtualRegNum upvalNum(int levelsUp, int which);

  // Allocate a temporary local variable. Returns a local number (casts to int), which can be
  // passed to pushlocal(), etc. Strictly, freetemp is optional.
  VirtualRegNum newTemp();
  void freeTemp(VirtualRegNum which);

  void pushInt(int i);
  void pushDouble(double d);
  // The string pointer must remain valid until after the resulting CompiledScript is destroyed
  void pushStr(const char *str);
  // Push a 'virtual' register. Use values from localNum(), newTemp(), etc
  void pushLocal(VirtualRegNum which);
  // Apply an operator to the top stack contents and push result. Variable arity
  void op(OperatorNum op);
  // Call opStart before pushing/computing the arguments for an operator
  void opStart(OperatorNum op);
  // Call funcStart before pushing/computing the arguments, and func afterwards
  void funcStart(int id, int argc, bool returnval = true);
  void func(int id, int argc, bool returnval = true);

  // Push a script function onto the stack. Can be a subscript.
  void pushScript(int id);

  // Call callStart(), push the function object followed by the arguments,
  // then call call() afterwards.
  void callStart(int argc, bool returnval = true);
  void call(int argc, bool returnval = true);

  // This is required after an expression which we want to discard the
  // result of (pops top of stack). Should not be called after assignment
  // or flow control. DO call after func().
  void endStatement();
  //void pop(); //discard top of stack
  // setlocal pops stack top, copytolocal preserves it
  void setLocal(VirtualRegNum which);
  void copyToLocal(VirtualRegNum which);

  // if is not allowed in an expression.
  void ifStart();  // Call before starting condition expression
  void ifCond();   // Call after condition is pushed
  BaseOpStream *ifThenStream(); // Stream to which to push 'then' block
  BaseOpStream *ifElseStream(); // Stream to which to push 'else' block
  void ifEnd();    // Call after then and else are translated
};

#if defined(H4_VM)
  #include "ir_h4.hpp"
  //typedef H4OpStream OpStream;
#elif defined(LUATEXT_VM)
  #include "ir_luatext.hpp"
  //typedef LuaTextOpStream OpStream;
#else
  #error "No interpreter backend selected"
#endif

#endif
