
#ifndef UTIL_H
#define UTIL_H

#ifdef __cplusplus
extern "C" {
#endif


struct array {
  int *p;
  int end;
  int allocd;
};

void array_new(struct array *a);
void array_push(struct array *a, int val);

/******************************** Memory Pool ********************************/

/* Each memchunk struct is located somewhere within the allocated chunk memory;
 * base holds the real start of the chunk. */ 
struct memchunk {
  struct memchunk *next;
  void *base;
};

struct mempool {
  int alignment;
  int blocksize;
  int chunksize;
  void **freelist;
  struct memchunk *chunklist;
};
typedef struct mempool mempool_t;

mempool_t *mempool_new(int blksize);
void *mempool_allocblk(mempool_t *pool);
void mempool_freeblk(mempool_t *pool, void *blk);


#ifdef __cplusplus
}
#endif

#endif
