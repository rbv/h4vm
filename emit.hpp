#ifndef EMIT_HPP
#define EMIT_HPP

#include <stddef.h>
#include <stdio.h>
#include <vector>
#include <string>

#include "interpreter.h"
#include "ir_h4.hpp"
#include "instructions.hpp"

/* ENCODING TRACKING
 * each basic type (int, string, etc) may be stored in a 32 bit variable in one
 * of two FORMS: encoded (see interpreter.h) and decoded (which for most types
 * is a pointer, or plain int for ints)
 * A value is TYPED if its encoded form is different from its decoded form -
 * everything except 'small ints'.
 * A value is MAYBEENC if it may or may not be encoded - but this must be
 * distinguishable (by the interpreter). An int is MAYBEENC if it is either
 * a small int or an encoded low int. It CAN'T be either an encoded or decoded
 * low int at once - that is not distinguishable.
 * IMPORTANT convention: if a value may or may not be typed, then all typed values
 * MUST be encoded - therefore a only-MAYBETYPED value MUST be ENCODED or DECODED: 
 * MAYBETYPED | TYPED | MAYBEENC | ENC  : normal encoded value
 * MAYBETYPED | TYPED | MAYBEENC  : either encoded or decoded typed value, illegal for integers
 * MAYBETYPED | TYPED | DECODED  : decoded value, of known type
 * MAYBETYPED | MAYBEENC | ENC  : set of possible values including small ints, all encoded
 * MAYBETYPED | DECODED  : a plain int which might be a low int, illegal for non integers
 * MAYBETYPED | all else  : illegal
 * TODO: SO MAYBEENC IS PROBABLY TOO MUCH TROUBLE TO HAVE
 */

#define _vctTYPEMASK  0xf

enum vbit_t {
  // Variable flags: independent of type
  vbitREFC	= 0x10, // This VARIABLE (NOT this TYPE) counts as a reference
  //vbitEXACT	= 0x20, // Exact value known
  //at least one of the following must be set, in the logical way
  vbitDECODED	= 0x40, // either a plain int or a direct pointer to other type
  vbitENCODED	= 0x80, // has been encoded (does not include unencoded ints)
  vbitMAYBEENC	= 0x100, // might have been encoded

  vbitFREE	= 0x200, // this variable is not in use: can be allocated
  vbitMODIFIED	= 0x400, // modified since whenever this bit was cleared

  // Variable content type flags
  vbitTYPED	= 0x10000, //not a small int - requires encoding
  vbitMAYBETYPED= 0x20000, //might be a value which requires encoding
  vbitNUMBER	= 0x40000,
  /*
  vbitINT	= 0x8000 | vbitNUMBER,
  vbitDECODEDINT= 0x10000 | vbitINT,
  */
};

// Why is FREE a bit and NULL a type? Possible rationale: there are multiple
// NULL (empty) value states: currently have two, vctFREE and vctNULL.
// But vbitFREE is not a real bit: there can only be one FREE state.
enum vctype_t {
  vtNULL	= 0,
  vtANY		= 1,
  vtINT		= 2,
  vtSTRING	= 3,
  vtDOUBLE	= 4,
  vtARRAY	= 5,
  vtUDT		= 6,
};

#define TYPED_N_ENC  vbitMAYBETYPED | vbitTYPED | vbitMAYBEENC | vbitENCODED | vbitMODIFIED
enum vccompound_t {
  // The following common 'compound' type descriptions are used to initialise
  // VarContents. They should never be compared with a VarContent.
  // All of these include vbitMODIFIED because setting a variable content is
  // assumed to mean that its type has changed.
  vctFREE	= vtNULL | vbitFREE | vbitMODIFIED, // Ignoring MODIFIED differences, the only valid FREE state.
  vctNULL	= vtNULL | vbitMODIFIED, // "Allocated", but not yet assigned to. FIXME: TYPED flags?
  vctANY	= vtANY | vbitMAYBETYPED | vbitMAYBEENC | vbitMODIFIED,
  vctSML_INT	= vtINT | vbitDECODED | vbitNUMBER | vbitMODIFIED, // Doesn't need to be encoded as a LOW_INT
  vctPLAIN_INT	= vtINT | vbitMAYBETYPED | vbitDECODED | vbitNUMBER | vbitMODIFIED,
  vctENC_INT	= vtINT | vbitMAYBETYPED | vbitMAYBEENC | vbitENCODED | vbitNUMBER | vbitMODIFIED, //T_SML_INT or T_LOW_INT
  vctENC_STRING	= vtSTRING | TYPED_N_ENC,
  vctENC_DOUBLE	= vtDOUBLE | TYPED_N_ENC | vbitNUMBER,
  vctENC_ARRAY	= vtARRAY | TYPED_N_ENC,
  vctENC_UDT	= vtUDT | TYPED_N_ENC,
};

class VarContent {
public:
  VarContent(enum vccompound_t templ = vctNULL/*, bool defaultFlags = true*/) : bits(templ) {
    /*
    if (defaultFlags) {
      if (bits & vbitMAYBETYPED)
	bits |= vbitMAYBEENC | vbitREFC;
      else if (bits & vbitTYPED)
	bits |= vbitENCODED | vbitMAYBEENC | vbitREFC;
      else
	bits |= vbitDECODED;
    } else
      // must still make sure encoding flags are set validly
      bits |= vbitMAYBEENC;
    */
  }
  enum vctype_t type() const { return (vctype_t)(bits & _vctTYPEMASK); }
  bool isFree() const { return is(vbitFREE); }
  bool isNull() const { return type() == vtNULL; }
  bool modified() const { return is(vbitMODIFIED); }
  void modify() { bits |= vbitMODIFIED; }
  void unmodify() { bits &= ~vbitMODIFIED; }
private:
  void markDecoded() { bits = (bits & ~(vbitENCODED | vbitMAYBEENC)) | vbitDECODED; }
  void markEncoded() { bits = (bits & ~vbitDECODED) | vbitMAYBEENC | vbitENCODED; }
  void markMaybeEnc() { bits = (bits & ~(vbitENCODED | vbitDECODED)) | vbitMAYBEENC; }

  void markMaybeTyped() { bits = (bits & ~vbitTYPED) | vbitMAYBEENC; }
public:
  /*
  void setDecoded() { bits = (bits & ~(vbitENCODED | vbitMAYBEENC)) | vbitDECODED; }
  void setEncoded() { 
    if (is(vbitTYPED))
      markEncoded();
    else if (is(vbitMAYBETYPED))
      markMaybeEnc();
    else
      assert(false);
  }
  void setMaybeEnc() {}
  */

  //FIXME: unfinished
  bool needEncode() const {
    if (is(vbitMAYBETYPED))
      return !is(vbitENCODED);
    else
      return is(vbitDECODED);
  }
  bool needDecode() const {
    return !is(vbitDECODED);
  }


  //VarContent operator|(const VarContent &rhs) const { return VarContent(bits | rhs.bits); } //WRONG
  //VarContent &operator|=(const VarContent &rhs) { bits |= rhs.bits; return *this; }

  //  int operator&(const int rhs) const { return bits & rhs; }
  bool operator&(const enum vbit_t rhs) const { return bits & rhs; }
  bool isAny(const int mask) const { return bits & mask; }
  bool isAll(const int mask) const { return (bits & mask) == mask; }
  int is(const int mask) const { return bits & mask; }

  void unionWith(const VarContent &rhs);

  operator int() const { return bits; }

  std::string toString() const;

private:
  void initWith(enum vccompound_t templ) { bits = templ; }
  void addBits(const int mask) { bits |= mask; }

  int bits;
};

/*
class regarray {
  vector<VarContent> vec;
  unsigned int stacksz;
}
*/

class EmitState;

class VmModel {
public:
  // temporaries is the number of virtual temporary variables to track
  VmModel(int args, int locals, int temporaries);

  void print() const;

  void unionWith(const VmModel &rhs);
  bool typesEqual(const VmModel &rhs) const;
  void clearModifiedFlags();

  // Walk opStream starting from ip until the first branching Op, updating state.
  // If st is given, emit bytecode. Returns that branching Op, or opStream->end()
  H4OpStream::iterator processStream(H4OpStream *opStream, H4OpStream::iterator ip, EmitState *st = NULL);

  void emptyGen(EmitState *st = NULL);
  void pushGen(EmitState *st = NULL);
  void moveToGen(const Operand &operand, EmitState *st = NULL);

  Operand allocTempVariable();
  void deleteTempVariable(Operand which);

  void emitOp(OperatorInstr *ip, EmitState *st = NULL);
  void emitCall(FunctionInstr *ip, EmitState *st = NULL);

  // Dereference virtual stack and temporaries indirection
  Operand &decodeArg(Operand &operand);
  const Operand &decodeArg(const Operand &operand) {
    return const_cast<const Operand &>(decodeArg(const_cast<Operand &>(operand)));
  }

  // Get operand register, including virtual stack and temporaries indirection
  VarContent &getReg(const Operand &operand);

  OperandTy getRegType(const Operand &operand) const {
    // TODO OPTIMISATION: see decodeArg
    if (operand.type() == regtyVSTACK)
      return vstack[operand.num()].type();
    if (operand.type() == regtyVTEMP)
      return vtemplist[operand.num()].type();
    return operand.type();
  }

  // The special 'gen' register
  VarContent gentype;

  // Tracks the state of the real VM registers, not including stack portion
  std::vector<VarContent> regs;

  // The state of the real VM registers used as a stack
  // may be larger than stacksz: track contents past the end of the stack (idiotic?)
  std::vector<VarContent> stk;

  // Actual stack size (position in stk of next pushed element) DELETEME
  unsigned int stacksz;

  // Tracks the state of the virtual IR temporary variable pool
  // TODO: This is a bit wasteful since each temp is only used for a short time:
  // might cause bad performance in unionWith?
  // May only be either regtyREAL or regtySTACK
  std::vector<Operand> vtemplist;

  // Tracks the state of the virtual IR stack
  // May only be either regtyREAL or regtySTACK
  std::vector<Operand> vstack;

private:
  // Check that all register lists are the same length, or whatever necessary
  // to completely compare two instances. This is really handwavy because I
  // don't know very well what is needed.
  void assertUnifiable(const VmModel &rhs) const;

  /*
  class Dbg {
  public:
    static int count;
    int sanity;
    Dbg() : sanity(1) { count++; printf("\nNEW VmModel (%d)\n", count); }
    Dbg(const Dbg &) : sanity(2) { count++; printf("\nCOPIED VmModel (%d)\n", count); }
    ~Dbg() { sanity = -1; count--; printf("\nDELETED VmModel (%d)\n", count); }
  } dbg;
  */
};

class EmitState {
public:
  EmitState(int args, int locals, H4OpStream *stream);
  ~EmitState();

  // After this is called, the EmitState is closed and unusable.
  // Caller must delete the CompiledScript.
  H4CompiledScript *emitCode(H4OpStream *stream);

  // Similar to PUSH_STKREG macro. Decodes vstack indirection
  void emitOperand(const Operand &operand);

  VmModel *state;

  char *code; // Beginning of buffer
  char *p;    // Current write position in buffer
  int codealloc; //bytes

  int argc;
  int localc;
  //int initialsp; //stackp is set before the script is executed

  std::vector<int> argstk;

  //PUSH_STKREG doesn't push the real register number because it not known yet
  //how many temporary registers there are (state.regs.size()). Need to correct
  //them afterwards using this list of the offsets in 'code' of the register #'s
  std::vector<ptrdiff_t> stk_reg_addrs;

private:
  // emit: when false do register remapping; else emit bytecode
  void processStream(H4OpStream *opStream, bool emit);

  // emit: whether to emit bytecode
  void branchIf(IfInstr &ifIn, bool emit);
};

#endif
