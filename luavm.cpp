
/*
 * This module contains code not specific to the lua source- or bytecode-
 * emitting IR backends.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "interpreter.h"
#include "ir_luatext.hpp"

using namespace std;

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#ifdef USE_LUAJIT
  #include <luajit.h>
#endif

/*  Lua 5.1 doesn't have lua_rawlen(), but instead lua_objlen() */
#if LUA_VERSION_NUM < 502
  #define lua_rawlen lua_objlen
#endif

}

#define MAX_FUNC_ARGUMENTS 10

struct LuaVMState {
  lua_State *L;
} vmstate;

static int lvm_func(lua_State *L) {
  int argc = lua_gettop(L) - 1;
  assert(argc < MAX_FUNC_ARGUMENTS);
  int id = luaL_checkinteger(L, 1);
  int args[MAX_FUNC_ARGUMENTS];
  int typemask = 0;
  for (int argi = 0; argi < argc; argi++) {
    // Skip the 'id' argument: args start at stack pos 2
    // FIXME: this probably doesn't belong here
    // booleans are not numbers. toboolean returns 1 for anything other than nil and false
    if (lua_isboolean(L, 2 + argi))
      args[argi] = lua_toboolean(L, 2 + argi);
    else
      args[argi] = luaL_checkint(L, 2 + argi);
  }
  int ret = functions_callback(id, argc, args, typemask);
  lua_pushinteger(L, ret);
  return 1;
}

luaL_Reg interface_functions[] = {
  {"func",   lvm_func},
  {NULL,     NULL}
};

// Push onto the stack the environment that we use for compiled chunks:
// just the default global environment _G for now
void push_chunk_environment(lua_State *L) {
  #if LUA_VERSION_NUM <= 501
    // lua5.1 and luajit2.0
    lua_pushvalue(L, LUA_GLOBALSINDEX);
  #else
    lua_rawgeti(L, LUA_REGISTRYINDEX, LUA_RIDX_GLOBALS);
  #endif
}

void init_interpreter() {
  printf("Initializing " LUA_RELEASE "\n");

  #ifdef USE_LUAJIT
    printf(LUAJIT_VERSION "\n");
    // Does nothing, but causes a dynamic linker error if linked to wrong .so
    LUAJIT_VERSION_SYM();
  #endif

  lua_State *L = luaL_newstate();
  vmstate.L = L;

  luaL_openlibs(L); // Standard libraries

  push_chunk_environment(L);
  int chunkenv = lua_gettop(L);

  // Create the table of compiled scripts
  lua_newtable(L);
  lua_setfield(L, chunkenv, "scripts");

  // This is similar to luaL_register, but that was deprecated in lua 5.2
  luaL_Reg *func_to_reg = interface_functions;
  while (func_to_reg->name) {
    lua_pushcfunction(L, func_to_reg->func);
    lua_setfield(L, chunkenv, func_to_reg->name);
    func_to_reg++;
  }

  // TODO: load the error handling function

  lua_pop(L, 1);
}

void shutdown_interpreter() {
  lua_close(vmstate.L);
  vmstate.L = NULL;
}

void printvariable(int var) {
}


/******************************************************************************/

// buf can actually contain either source or bytecode
LuaCompiledScript::LuaCompiledScript(int id, string &buf) {
  printf("COMPILING: '''%s'''\n", buf.c_str());

  lua_State *L = vmstate.L;
  push_chunk_environment(L);
  int chunkenv = lua_gettop(L);
  lua_getfield(L, -1, "scripts");  // push scripts[]

  int res = luaL_loadbuffer(vmstate.L, buf.c_str(), buf.size(), "=chunk1");
  if (res != 0) {
    // if (res == LUA_ERRSYNTAX)
    fprintf(stderr, "lua_load error: %s\n", lua_tostring(L, -1));
    lua_pop(L, 3);  // pop environment, scripts[], error message
    return;    // FIXME
  }

  // Execute the chunk, returning the real function/closure
  // TODO: error handler
  res = lua_pcall(vmstate.L, 0, 1, 0);  // pop chunk, push result
  if (res != 0) {
    // chunk was popped
    fprintf(stderr, "error while calling wrapping chunk: %s\n", lua_tostring(L, -1));
    lua_pop(L, 3);  // pop environment, scripts[], error message
    return;    // FIXME
  }

  // Maybe set the environment upvalue?
  lua_pushvalue(L, chunkenv);
  #if LUA_VERSION_NUM <= 501
    // including luajit
    lua_setfenv(L, -2);  // set fenv = chunk_environment
  #else
    lua_setupvalue(L, -2, 1);  // set first upvalue
  #endif

  // Set it in the scripts table
  lua_rawseti(L, -2, id);  // pops function
  lua_pop(L, 2);  // pop environment, scripts[]

  _id = id;
}

bool LuaCompiledScript::run(int *locals) {
  //printf("hello: '%s'\n", textbuf.c_str());
  lua_getglobal(vmstate.L, "scripts");
  lua_rawgeti(vmstate.L, -1, _id);
  //lua_getglobal(vmstate.L, "err_handler");
  int res = lua_pcall(vmstate.L, 0, 0, 0);  // 0 args, 0 results
  if (res != 0) {
    fprintf(stderr, "lua_pcall error (script %d): %s\n", _id, lua_tostring(vmstate.L, -1));
    return false;
  }
  
  printf("success.\n");
  return true;
}
