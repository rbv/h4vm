
#ifndef IR_LUATEXT_HPP
#define IR_LUATEXT_HPP

#include <string>
#include <vector>
#include <assert.h>

#include "ir.hpp"

enum ExpType {
  expVal,    // $str
  expFunc,   // arg0(arg1...)  ($str not used)
  expInfix,  // arg0 $str arg1
};
struct Expression {
  Expression(ExpType type_, std::string str_, int argc_ = 0)
    : type(type_), str(str_), argc(argc_) {}
  ExpType type;
  std::string str;
  int argc = 0;
  //bool sideeffect_safe;  // Does not change meaning if any variables are modified

};

// A compiled script
class LuaCompiledScript : public CompiledScript {
public:
  ~LuaCompiledScript() {}

  // (Placeholder only) True if no error occurred
  bool run(int *locals);

private:
  LuaCompiledScript(int id, std::string &buf);

  int _id;  // index in scripts[] lua array

  friend class LuaTextOpStream;
};


class LuaTextOpStream : public BaseOpStream {
public:

  LuaTextOpStream(int id_, int args_, int locals_, int upvals_);
  ~LuaTextOpStream();

  CompiledScript *emit();

  // Return the register number for the i-th...
  VirtualRegNum localNum(int which);
  VirtualRegNum argNum(int which);
  VirtualRegNum upvalNum(int levelsUp, int which);

  // Allocate a temporary local variable. Returns a local number (casts to int), which can be
  // passed to pushlocal(), etc. Strictly, freetemp is optional.
  VirtualRegNum newTemp();
  void freeTemp(VirtualRegNum which);

  void pushInt(int i);
  void pushDouble(double d);
  void pushStr(const char *str);
  // Push a 'virtual' register. An int >= 0 may be passed to specify a local
  // variable, otherwise a value returned from newtemp
  void pushLocal(VirtualRegNum which);
  // Apply an operator to the top stack contents and push result. Variable arity

  void op(OperatorNum op);
  // Call opStart before pushing/computing the arguments for an operator
  void opStart(OperatorNum op);
  // Call funcStart before pushing/computing the arguments, and func afterwards
  void funcStart(int id, int argc, bool returnval = true);
  void func(int id, int argc, bool returnval = true);

  // Push a script function onto the stack. Can be a subscript.
  void pushScript(int id);

  // Call callStart(), push the function object followed by the arguments,
  // then call call() afterwards.
  void callStart(int argc, bool returnval = true);
  void call(int argc, bool returnval = true);

  // This is required after an expression which we want to discard the
  // result of (pops top of stack). Should not be called after assignment
  // or flow control.
  void endStatement();

  //void pop(); //discard top of stack
  // setlocal pops stack top, copytolocal preserves it
  void setLocal(VirtualRegNum which);
  void copyToLocal(VirtualRegNum which);


  void ifStart();  // Call before starting condition expression
  void ifCond();   // Call after condition is pushed
  LuaTextOpStream *ifThenStream(); // Stream to which to push 'then' block
  LuaTextOpStream *ifElseStream(); // Stream to which to push 'else' block
  void ifEnd();    // Call after then and else are translated

private:
  //std::vector<std::string> str_stack;
  std::string textbuf;  // Contains fully constructed statements

  // If true, prettify the source
  bool debug;

  int indent_level;

  bool insideExpression = false;

  //std::vector<std::string> statement_stack;

  std::vector<Expression> exp_stack;
  int nexttemp;

  int id;  // script id number

  // Number of:
  int args, locals, upvals;

  std::string getRegName(VirtualRegNum which);

  // Return a string for the top expression on the stack, and pop it.
  std::string popExp();

  void indent();

  void pushVal(std::string val) {
    exp_stack.push_back(Expression(expVal, val));
    insideExpression = true;
  }

  Expression &curExp() {
    assert(exp_stack.size() >= 1);
    return exp_stack.back();
  }
};


typedef LuaTextOpStream OpStream;
//class OpStream : public LuaTextOpStream {};

#endif
