#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#define REGTY_NAMES
#define SOP_NAMES
//#include "interpreter.h"
#include "emit.hpp"
#include "ir_h4.hpp"
#include "instructions.hpp"

using namespace std;


// Operator descriptors
struct IROperator
/*		name		flags	argc	op_any		op_int			op_encint */
opdAdd =	{"Add",		0,	2,	opADD_ANY_ANY,	opADD_INT_INT,		opNIL},
opdCastDouble = {"CastDouble",	0,	1,	opNIL,		opCAST_DOUBLE,		opNIL}
;



string Operand::toString() const {
  char buf[32];
  //assert(type() >= 0 && type() < regty_MAX);
  if (type() < 0 || type() >= regty_END)
    sprintf(buf, "<illegal %d:%d>", type(), num());
  else if (type() == regtyGEN || type() == regtyNONE)
    strcpy(buf, regty_names[type()]);
  else
    sprintf(buf, "%s:%d", regty_names[type()], num());
  return string(buf);
}

H4OpStream::H4OpStream(int id_, int args_, int locals_, int upvals_, H4OpStream *parent_) {
  id = id_;
  args = args_;
  locals = locals_;
  upvals = upvals_;
  parent = parent_;
  closed = false;
  length = 0;
  last = iterator((OpCode *)&stream[0]);
  assert(last == end());

  // Sharing tempCounter may not actually be necessary in all circumstances.
  if (parent) {
    stacksz = parent->stacksz;
    tempCounter = parent->tempCounter;
  } else {
    stacksz = 0;
    tempCounter = new int(0);
  }
}

H4OpStream::~H4OpStream() {
  for (H4OpStreamIterator it = begin(); it != end(); ++it)
    it->~OpCode();

  if (!parent)
    delete tempCounter;
}

void H4OpStream::print(int indent) {
  if (indent == 0) {
    printf("%*.0d" "H4OpStream (length %d, %d bytes (%d bytes memory usage)):\n",
	   indent, 0, size(), (int)stream.size(), calcMemUsage());
  }
  H4OpStreamIterator it;
  for (it = begin(); it != end(); ++it) {
    printf("%*.0d %-10s", indent, 0, sop_names[it->type]);
#ifdef DEBUG_OPSTREAM
    printf(" stacksz=%-3d  ", it->stacksz_was);
#endif
    switch(it->type) {
    case sop_int:
      printf("%d", it->castInt().i);
      break;
    case sop_double:
      printf("%f", it->castFloat().d);
      break;
    case sop_cstr:
      printf("%s", it->castString().str);
      break;
    case sop_reg:
    case sop_setreg:
      printf("%s", it->castUnOp().arg.toString().c_str());
      break;
    case sop_varbegin:
    case sop_varend:
      printf("%d", it->castReg().reg);
      break;
    case sop_func:
      {
	FunctionInstr &f = it->castFunction();
	printf("id:%d args:%d ", f.id, f.argc);
	for (int i = 0; i < f.argc; i++)
	  printf(" %s", f.args[i].toString().c_str());
      }
      break;
    case sop_op:
      {
	OperatorInstr &o = it->castOperator();
	printf("%s", o.op->name);
	for (int i = 0; i < o.op->argc; i++)
	  printf(" %s", o.args[i].toString().c_str());
      }
      break;
    case sop_if:
      {
	IfInstr &i = it->castIf();
	printf("\n");
	if (i.thenStream) {
	  printf("%*.0d then stream:\n", indent, 0);
	  i.thenStream->print(indent + 2);
	}
	if (i.elseStream) {
	  printf("%*.0d else stream:\n", indent, 0);
	  i.elseStream->print(indent + 2);
	}
      }
      continue;  // Skip printf("\n")
    case sop_undefined:
      fatal("fff");
      break;
    default:
      break;
    }
    printf("\n");
  }
}

int H4OpStream::calcMemUsage() {
  int sum = (int)stream.capacity() + sizeof(H4OpStream);
  for (H4OpStreamIterator it = begin(); it != end(); ++it)
    sum += it->calcMemUsage();
  return sum;
}

/*
int aligned_sizeof(size_t sz, size_t alignment) {
  if (sz & (alignment - 1))
    return (sz & ~(alignment - 1)) + alignment;
  return sz;
}
*/

inline int alignmentPadding(size_t val, size_t alignment) {
  if (val & (alignment - 1))
    return alignment - (val & (alignment - 1));
  return 0;
}

void *H4OpStream::alloc(size_t sz, size_t alignment) {
  // TODO: Use alignment properly. For now, am lazy and just pad everything to
  // a multiple of 8, which should the maximum alignment required
  //printf("\naligning %d to", sz);
  sz += alignmentPadding(sz, 8);
  //printf(" %d\n", sz);
  int newLastPos = stream.size();
  int lastOpSize;
  if (size())
    lastOpSize = ((_H4OpStreamItem *)&*last)->size;
  else
    lastOpSize = 0;
  stream.resize(stream.size() + sz);

  _H4OpStreamItem *ret = (_H4OpStreamItem *)&stream[newLastPos];
  ret->size = sz;
  ret->prevsize = lastOpSize;
  last = iterator((OpCode *)ret);
  length += 1;
  return ret;
}

void H4OpStream::append(OpCode *code) {
  assert(code == last);
  if (closed)
    fatal("Wrote to closed H4OpStream");
#ifdef DEBUG_OPSTREAM
  last->stacksz_was = stacksz;
#endif
}

CompiledScript *H4OpStream::emit() {
  this->print();
  printf("H4OpStream::emit():\n");

  EmitState *st = new EmitState(args, locals, this);
  H4CompiledScript *ret = st->emitCode(this);
  printf("\nemitted, dumping:\n");
  disassembleBC(st->code, st->p);
  delete st;
  return ret;
}

void H4OpStream::pushInt(int i) {
  stacksz++;
  append(new(this) IntInstr(i));
}

void H4OpStream::pushDouble(double d) {
  stacksz++;
  append(new(this) FloatInstr(d));
}

void H4OpStream::pushStr(const char *str) {
  stacksz++;
  append(new(this) StringInstr(str));
}

// TODO: OPTIMISATION: turn all local variables except for arguments
// into temporaries
void H4OpStream::pushLocal(VirtualRegNum which) {
  stacksz++;
  append(new(this) UnaryInstr(sop_reg, VirtualRegister(which).toOperand()));
}

void H4OpStream::pushScript(int id) {
  // TODO
  assert(false);
}

// Translate from to OperatorNums to IROperator*s
IROperator *irOpTable[] = {
  NULL,         // opnumInvalid
  &opdAdd,	// opnumAdd
  NULL,         // opnumSub
  NULL,         // opnumMult
  NULL,         // opnumDivInt
  NULL,         // opnumDivFloat
  NULL,         // opnumMod
  NULL,         // opnumEqual
  NULL,         // opnumInequal
};

void H4OpStream::opStart(OperatorNum opnum) {
  // Nothing.
}

void H4OpStream::op(OperatorNum opnum) {
  // We set stacksz to the new stack size before calling append(), because
  // append() may record it;
  if (opnum <= opnumInvalid || opnum >= opnumNUM)
    fatal("H4OpStream: invalid operator number %d", opnum);
  assert(opnum < sizeof(irOpTable) / sizeof(irOpTable[0]));
  IROperator *op = irOpTable[opnum];
  stacksz += -op->argc + 1;
  append(new(this, op) OperatorInstr(op, stacksz - 1));
}

void H4OpStream::funcStart(int id, int argc, bool returnval) {
  // Nothing.
}

void H4OpStream::func(int id, int argc, bool returnval) {
  stacksz += -argc + 1;
  append(new(this, argc) FunctionInstr(id, argc, stacksz - 1));
  if (!returnval)
    pop();
}

void H4OpStream::callStart(int argc, bool returnval) {
  // Nothing.
}

void H4OpStream::call(int argc, bool returnval) {
  // TODO:  not implemented
  assert(false);
}

void H4OpStream::pop() {
  stacksz--;
  append(new(this) OpCode(sop_pop));
}

void H4OpStream::endStatement() {
  // Implementing this is quite optional and seems like a total waste of
  // time, since func() already calls pop as needed, and that will
  // cover 99.99% of cases, and in the other 0.01% the stuff will get
  // popped at the end of the block.
}

void H4OpStream::setLocal(VirtualRegNum which) {
  append(new(this) UnaryInstr(sop_setreg, VirtualRegister(which).toOperand()));
  pop();
}

void H4OpStream::copyToLocal(VirtualRegNum which) {
  append(new(this) UnaryInstr(sop_setreg, VirtualRegister(which).toOperand()));
}

VirtualRegNum H4OpStream::localNum(int which) {
  assert(0 <= which && which < locals);
  return args + which;
}

VirtualRegNum H4OpStream::argNum(int which) {
  assert(0 <= which && which < args);
  return which;
}

VirtualRegNum H4OpStream::upvalNum(int levelsUp, int which) {
  assert(0 <= which && which < upvals);
  //return args + locals + which;
  assert(false); // TODO
}

VirtualRegNum H4OpStream::newTemp() {
  append(new(this) RegInstr(sop_varbegin, *tempCounter));
  return VirtualRegister((*tempCounter)++, true);
}

void H4OpStream::freeTemp(VirtualRegNum which) {
  append(new(this) RegInstr(sop_varend, VirtualRegister(which).toVTemp()));
}

void H4OpStream::ifStart() {
  // Nothing needed.
}

void H4OpStream::ifCond() {
  stacksz--;
  append(new(this) IfInstr);
  closed = true;
}

void H4OpStream::ifEnd() {
  if (!size() || back().type != sop_if)
    fatal("H4OpStream::endif not inside if block");
  IfInstr &ifIn = back().castIf();
  // TODO: might be worth checking here (already done in emitter) that both
  // streams have same stacksz
  if (ifIn.thenStream) {
    if (ifIn.thenStream->size() == 0) {
      // A small optimisation
      delete ifIn.thenStream;
      ifIn.thenStream = NULL;
    } else {
      ifIn.thenStream->closed = true;
      stacksz = ifIn.thenStream->stacksz;
    }
  }
  if (ifIn.elseStream) {
    if (ifIn.elseStream->size() == 0) {
      // A small optimisation
      delete ifIn.elseStream;
      ifIn.elseStream = NULL;
    } else {
      ifIn.elseStream->closed = true;
      stacksz = ifIn.elseStream->stacksz;
    }
  }
  closed = false;
}
 
H4OpStream *H4OpStream::ifThenStream() {
  if (!size() || back().type != sop_if
      || !closed) // Check whether endif() called already
    fatal("H4OpStream: ifThenStream called but current instruction is not an if");

  IfInstr &ifIn = back().castIf();
  if (ifIn.thenStream)
    return ifIn.thenStream;
  return ifIn.thenStream = new H4OpStream(-1, 0, locals, upvals, this);
}

H4OpStream *H4OpStream::ifElseStream() {
  if (!size() || back().type != sop_if
      || !closed) // Check whether endif() called already
    fatal("H4OpStream: ifElseStream called but current instruction is not an if");

  IfInstr &ifIn = back().castIf();
  if (ifIn.elseStream)
    return ifIn.elseStream;
  return ifIn.elseStream = new H4OpStream(-1, 0, locals, upvals, this);
}
