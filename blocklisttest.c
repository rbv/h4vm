#include <stdio.h>

#include "util.h"
#include "blocklist.h"

int main() {

  blocklist_t *bl;

  bl = blocklist_new();
  int i;
  int *dat;
  for (i = 0; i < 100; i++) {
    dat = bl_append(bl, 4);
    *dat = i;
  }
  dat = bl_append(bl, 4);
  *dat = 68;


  /* Start at 50th and go backwards */
  dat = bl_first(bl);
  for (i = 0; i < 49; i++)
    dat = bl_next(dat);
  bl_remove(dat, 15);

  //for (; dat; dat = bl_prev(dat))
  //  printf ("%d, ", *dat);

  for (dat = bl_first(bl); dat; dat = bl_next(dat))
    printf ("%d, ", *dat);
  printf ("\n\n%d. %d\n\n",  *(int *)bl_last(bl), BLOCKDATA);



  return 0;
}
