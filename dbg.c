#include <string.h>
#include <assert.h>
#include <ctype.h>

#define WANT_OPDOCS
#include "opcodes.h"
#include "interpreter.h"

int getopid(void *op) {
  unsigned int i;
  for (i = 0; i < sizeof(opdocs) / sizeof(opdocs[0]); i++) {
    if (OPADDR(i) == op)
      return i;
  }
  return 0;
}

const char *getopcname(void *op) {
  unsigned int i;
  for (i = 0; i < sizeof(opdocs) / sizeof(opdocs[0]); i++) {
    if (OPADDR(i) == op)
      return opdocs[i].name;
  }
  return "<no name>";
}

void disassembleBC(void *code, void *code_end) {
  void *ptr = code;
  while (ptr < code_end) {
    int offset = ptr - code;
    struct opcode_descript *op = &opdocs[getopid(*(void **)ptr)];

    char *arg, *bufp, *outp;
    char argbuf[32];
    char buf[256];
    outp = buf;
    buf[0] = '\0';

    ptr += sizeof(OP_TYPE);
    for (arg = op->args; arg; arg = strchr(arg, ',')) {
      arg++;
      // Copy this arg into argbuf
      bufp = argbuf;
      while (*arg && *arg != ',' && *arg != ')')
	*bufp++ = *arg++;
      *bufp = '\0';
      char sepchar = ' ';  //*arg;
      int argsz;
      char *argname;

      argname = bufp;
      while (isalpha(argname[-1]))
	argname--;

      if (strchr(argbuf, '$')) {
	break;

      } else if (strstr(argbuf, "reg*")) {
	outp += sprintf(outp, "%s=" LOCAL_NUM_FMTSTR "%c", argname, *(LOCAL_NUM_TYPE *)ptr, sepchar);
	argsz = sizeof(LOCAL_NUM_TYPE);

      } else if (strstr(argbuf, "char *")) {
	outp += sprintf(outp, "%s=\"%.10s\"%c", argname, *(char **)ptr, sepchar);
	argsz = sizeof(char *);

      } else if (strchr(argbuf, '*')) {
	outp += sprintf(outp, "%s=0x%p%c", argname, *(void **)ptr, sepchar);
	argsz = sizeof(void *);

      } else if (strstr(argbuf, "int")) {
	outp += sprintf(outp, "%s=%d%c", argname, *(int *)ptr, sepchar);
	argsz = 4;

      } else if (strstr(argbuf, "short")) {
	outp += sprintf(outp, "%s=%hd%c", argname, *(short *)ptr, sepchar);
	argsz = 2;

      } else if (strstr(argbuf, "double")) {
	outp += sprintf(outp, "%s=%f%c", argname, *(double *)ptr, sepchar);
	argsz = 8;

      } else {
	break;
      }
      ptr += argsz;
    }
    if (strlen(buf) + strlen(op->args) < 39) {
      printf("%4d%19s %s  %-*s: %s\n", offset, op->name, op->args, 39 - strlen(op->args), buf, op->docstr);
    } else {
      printf("%4d%19s %s\n%24.0d%-41s: %s\n", offset, op->name, op->args, 0, buf, op->docstr);
    }
  }
}

