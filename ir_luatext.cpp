/*
 * See also luavm.cpp, which contains code for loading chunks
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>

#include "interpreter.h"
#include "ir_luatext.hpp"

using namespace std;


/******************************************************************************/

/*
  h(g(x), g() && f(h(), x, x := y := 3, g())
  
  would need to become

  local temp0 = g(x)
  local temp1 = g()
  if temp1 then
    local temp2 = h()
    local temp3 = x
    y = 3
    x = y
  end if
  h(temp0, temp1 && f(temp2, temp3, x, g())

*/


  // Need to make all previous expressions safe, by evaluating and putting in temps



// Translate from to OperatorNums to IROperator*s
const char *irOpTable[] = {
  NULL,       // opnumInvalid
  "+",        // opnumAdd
  "- ",       // opnumSub  (space to avoid --)
  "*",        // opnumMult
  "/",        // opnumDivInt
  "/",        // opnumDivFloat
  "%",        // opnumMod
  "==",       // opnumEqual
  "~=",       // opnumInequal
};

LuaTextOpStream::LuaTextOpStream(int id_, int args_, int locals_, int upvals_) {
  id = id_;
  args = args_;
  locals = locals_;
  upvals = upvals_;
  nexttemp = 0;
  insideExpression = false;
  indent_level = 1;
  debug = true;

  char temp[40];

  // Emit a chunk which should be executed to get a function.
  // This is needed in order to create functions with arguments (and later, upvalues)
  textbuf = "return function(";
  for (int idx = 0; idx < args; idx++) {
    if (idx > 0)
      textbuf += ", ";
    snprintf(temp, 40, "arg%d", idx);
    textbuf += temp;
  }
  textbuf += ")\n";

  // Missing args should default to 0 instead of nil
  for (int idx = args - 1; idx >= 0; idx--) {
    snprintf(temp, 40, " if arg%d==nil then arg%d=0\n", idx, idx);
    textbuf += temp;
  }
  for (int idx = 0; idx < args; idx++) {
    textbuf += " end";
  }
  if (args)
    textbuf += '\n';

  for (int idx = 0; idx < locals; idx++) {
    snprintf(temp, 40, " local local%d\n", idx);
    textbuf += temp;
  }
}

LuaTextOpStream::~LuaTextOpStream() {
}

CompiledScript *LuaTextOpStream::emit() {
  assert(!insideExpression);
  assert(exp_stack.size() == 0);

  textbuf += "end";
  return new LuaCompiledScript(id, textbuf);
}

VirtualRegNum LuaTextOpStream::localNum(int which) {
  assert(0 <= which && which < locals);
  return args + which;
}

VirtualRegNum LuaTextOpStream::argNum(int which) {
  assert(0 <= which && which < args);
  return which;
}

VirtualRegNum LuaTextOpStream::upvalNum(int levelsUp, int which) {
  assert(0 <= which && which < upvals);
  assert(false); // TODO: not implemented
  return args + locals + which;
}

VirtualRegNum LuaTextOpStream::newTemp() {
  char temp[16];
  snprintf(temp, 16, "local temp%d\n", nexttemp);
  indent();
  textbuf += temp;
  return nexttemp++;
}

void LuaTextOpStream::freeTemp(VirtualRegNum which) {
  // Nothing
}

string LuaTextOpStream::getRegName(VirtualRegNum which) {
  char temp[16];
  if (which >= 0) {
    if (which < args)
      snprintf(temp, 16, "arg%d", which);
    else {
      which -= args;
      if (which < locals)
        snprintf(temp, 16, "local%d", which);
      else {
        which -= locals;
        assert(which < upvals);
        snprintf(temp, 16, "upval%d", which);
      }
    }
  } else {
    which = -1 - which;
    assert(which < nexttemp);
    snprintf(temp, 16, "temp%d", which);
  }
  return string(temp);
}

void LuaTextOpStream::pushInt(int i) {
  char temp[16];
  snprintf(temp, 16, "%d", i);
  pushVal(string(temp));
}

void LuaTextOpStream::pushDouble(double d) {
  char temp[26];
  snprintf(temp, 26, "%f", d);
  pushVal(string(temp));
}

void LuaTextOpStream::pushStr(const char *str) {
  char temp[strlen(str) * 4 + 4];
  const char *ch;
  char *outp = temp;
  *outp++ = '"';
  for (ch = str; *ch; ch++) {
    if (*ch == '\\') {
      *outp++ = '\\';
      *outp++ = *ch;
    } else if (*ch == '"') {
      *outp++ = '\\';
      *outp++ = *ch;
    } else if (*ch == '\n') {
      *outp++ = '\\';
      *outp++ = 'n';
    } else if (*ch < 32) {
      outp += sprintf(outp, "\\%03d", (int)(unsigned char)*ch);
    } else {
      *outp++ = *ch;
    }
  }
  *outp++ = '"';
  *outp = '\0';
  pushVal(temp);
}

void LuaTextOpStream::pushLocal(VirtualRegNum which) {
  pushVal(getRegName(which));
}

void LuaTextOpStream::pushScript(int id) {
  char temp[30];
  snprintf(temp, 30, "scripts[%d]", id);
  pushVal(temp);
}

void LuaTextOpStream::opStart(OperatorNum opnum) {
  insideExpression = true;
}

void LuaTextOpStream::op(OperatorNum opnum) {
  if (opnum <= opnumInvalid || opnum >= opnumNUM)
    fatal("LuaTextOpStream: invalid operator number %d", opnum);
  assert(opnum < sizeof(irOpTable) / sizeof(irOpTable[0]));

  exp_stack.push_back(Expression(expInfix, irOpTable[opnum]));
}

void LuaTextOpStream::funcStart(int id, int argc, bool returnval) {
  // Discarding the result in the middle of an expression is not allowed,
  // but we check it in func().
  insideExpression = true;
  // Push the function id argument
  pushVal("func");
  char temp[16];
  snprintf(temp, 16, "%d", id);
  pushVal(string(temp));
}

// returnval not used, depend on endStatement instead
void LuaTextOpStream::func(int id, int argc, bool returnval) {
  assert((int)exp_stack.size() >= argc + 1);
  exp_stack.push_back(Expression(expFunc, string(), argc + 1));
}

void LuaTextOpStream::callStart(int argc, bool returnval) {
  // Nothing.
}

void LuaTextOpStream::call(int argc, bool returnval) {
  exp_stack.push_back(Expression(expFunc, string(), argc));
}

void LuaTextOpStream::indent() {
  if (!debug)
    return;
  for (int idx = 0; idx < 2 * indent_level; idx++)
    textbuf += ' ';
}

void LuaTextOpStream::endStatement() {
  assert(insideExpression);  // 
  indent();
  if (exp_stack.back().type == expFunc) {
    // In lua, unlike all other types of expressions, function calls
    // can be used as statements. So nothing needed.
  } else {
    // Assign to a dummy
    int dummy = nexttemp++;
    char temp[32];
    snprintf(temp, 32, "local temp%d = ", dummy);
    textbuf += temp;
  }
  textbuf += popExp();
  textbuf += '\n';
  assert(exp_stack.size() == 0);
}

string LuaTextOpStream::popExp() {
  assert(exp_stack.size() >= 1);
  // Enable recurse
  Expression exp = exp_stack.back();
  exp_stack.pop_back();
  if (exp_stack.size() == 0)
    insideExpression = false;  // A new statement begins

  switch(exp.type) {
    case expVal:
      return exp.str;
    case expInfix:
      {
        string rhs = popExp();
        string lhs = popExp();
        return '(' + lhs + exp.str + rhs + ')';
      }
    case expFunc:
      {
        // exp.str ignored
        string ret; //(exp.str + '(');
        for (int idx = 0; idx < exp.argc; idx++) {
          if (idx > 0)
            ret = ", " + ret;
          ret = popExp() + ret;
        }
        // if (!returnval) {
        //   // Discarding the result in the middle of an expression is not allowed
        //   assert(!insideExpression);
        // }
        return popExp() + '(' + ret + ')';
      }
    default:
      fatal("popExp: Bad expression type");
  }
}

void LuaTextOpStream::setLocal(VirtualRegNum which) {
  indent();
  textbuf += getRegName(which) + " = " + popExp() + "\n";
  assert(!insideExpression);
}

void LuaTextOpStream::copyToLocal(VirtualRegNum which) {
  fatal("Variable assignment as an expression is not supported yet.");
}

void LuaTextOpStream::ifStart() {
  assert(!insideExpression);
  // Nothing
}

void LuaTextOpStream::ifCond() {
  // Everything (including 0) except nil and false count as true in lua
  // FIXME: allow false and nil as false values too
  indent();
  textbuf += "if " + popExp() + " ~= 0 then\n";
  assert(!insideExpression);  // Empty exp_stack
  indent_level++;
}

LuaTextOpStream *LuaTextOpStream::ifThenStream() {
  return this;
}

// Doubles as closing the then, if any
LuaTextOpStream *LuaTextOpStream::ifElseStream() {
  assert(!insideExpression);  // Empty exp_stack
  indent_level--;
  indent();
  textbuf += "else\n";
  indent_level++;
  return this;
}

void LuaTextOpStream::ifEnd() {
  assert(!insideExpression);  // Empty exp_stack
  indent_level--;
  indent();
  textbuf += "end\n";
}
