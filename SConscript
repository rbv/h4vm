
########################################
# Configuration settings

# The easiest way to link lua is to set pkg_config_lua_package
# to the pkg-config package name, eg 'luajit', 'lua', 'lua52'.
# If you don't have a .pc file for lua, set it to '' and set LUA_* manually.
pkg_config_lua_package = ''
LUA_LIBS = ['lua']
LUA_LIBPATHS = []
LUA_INCPATHS = ['-I/usr/include/lua-5.1']
# Whether to use luajit2 extensions. Optional even if linking to luajit2
LUAJIT2 = False

CC = 'gcc'
CXX = 'g++'
FBC = 'fbc-1.04'
CPPFLAGS = "-g -Wall -Wextra -Wno-unused-label -D$VM_DEF"
CXXFLAGS = "-std=gnu++11 -Wno-unused-function -Wno-unused-parameter -fno-exceptions -fno-rtti"
FBFLAGS = "-g"


########################################
# Parse arguments

arch = ARGUMENTS.get ('arch', '32')
x64 = arch in ('64', 'x64', 'x86_64')
if not x64:
    CPPFLAGS += ' -m32'
else:
    FBFLAGS += ' -arch x86_64'


########################################
# Building

import os
rootdir = Dir('#').abspath + os.path.sep

exe_suffix = ''
import platform
if platform.system () == 'Windows':
    exe_suffix = '.exe'
    # Force use of gcc instead of MSVC++, so compiler flags are understood
    envextra = {'tools': ['mingw']}

env = Environment (FBFLAGS = FBFLAGS,
                   FBLINKFLAGS = ['-l', 'stdc++'],
                   LIBPATH = [],
                   LIBS = [],
                   CPPPATH = [],
                   FBC = FBC,
                   CC = CC,
                   CXX = CXX,
                   LINKFLAGS = '-m32',
                   CPPFLAGS = CPPFLAGS.split(),
                   CXXFLAGS = CXXFLAGS.split())
env['ENV']['PATH'] = os.environ['PATH']

# from ohrbuild import basfile_scan
# bas_scanner = Scanner (function = basfile_scan,
#                        skeys = ['.bas', '.bi'], recursive = True)
# env.Append (SCANNERS = bas_scanner)

basexe = Builder (action = '$FBC $FBFLAGS -x $TARGET $SOURCES $FBLINKFLAGS',
                  suffix = exe_suffix, src_suffix = '.bas')
env['BUILDERS']['BASEXE'] = basexe
env['BUILDERS']['Object'].add_action ('.bas', '$FBC -c $SOURCE -o $TARGET $FBFLAGS')

def targetname(source, prefix):
    "foo.cpp -> ${prefix}foo.o"
    return prefix + source.split('.')[0] + '.o'


########################################
# Lua

# Flags to compile against lua.
luaenv = env.Clone()  # Environment shared by everything that links to lua
if pkg_config_lua_package:
    luaenv.MergeFlags(["!pkg-config --libs --cflags " + pkg_config_lua_package])
else:
    luaenv['CPPFLAGS'] += LUA_INCPATHS
    luaenv['LIBS'] += LUA_LIBS
    luaenv['LIBPATH'] += LUA_LIBPATHS

for lib in luaenv['LIBS']:
    luaenv['FBLINKFLAGS'] += ['-l', lib]
for libpath in luaenv['LIBPATH']:
    luaenv['FBLINKFLAGS'] += ['-p', libpath]

if LUAJIT2:
    luaenv['CPPFLAGS'] += ['-DUSE_LUAJIT']


########################################
# Targets

SCRIPTS = env.Command (
    ['test.hs'], source = 'test.hss', action =
    ['/home/ralph/ohr/spare-git/hspeak -ykwf test.hss $TARGET',  # Compile without optimisations
     'rm -rf build/hsz',
     'unlump $TARGET build/hsz'])

# Does not compile
env.Program ('blocklisttest', ['blocklisttest.c', 'blocklist.c', 'util.c', 'misc.c'])

############## h4vm_test

H4VM_TEST_MODULES = [
    'main.cpp',
    'loop.c',
    'hsz2bc.cpp',
    'datatypes.c',
    'ir_h4.cpp',
    'instructions.cpp',
    'emitter.cpp',
    'util.c',
    'dbg.c',
    'misc.c',
    'loadscript.bas',
    'interpreter.cpp',
]

#VariantDir('build_h4vm', '.')
env['VM_DEF'] = 'H4_VM'
H4VM_TEST_OBJECTS = [env.Object (target = targetname(module, 'h4-'), source = module) for module in H4VM_TEST_MODULES]

H4VM_TEST = env.BASEXE (rootdir + 'h4vm_test', source = H4VM_TEST_OBJECTS)
Default (H4VM_TEST)

env.Alias ('h4test', source = H4VM_TEST, action =
           ['./h4vm_test -o test_result.txt',
            'cmp test_result.txt test_correct.txt',
            'rm test_result.txt',
            'echo "All tests passed."'])
AlwaysBuild ('h4test')
Depends ('h4test', SCRIPTS)

############## luavm_test

LUAVM_TEST_MODULES = [
    'luavm_test.cpp',
    'luavm.cpp',
    'hsz2bc.cpp',
    'ir_luatext.cpp',
    'util.c',
    'misc.c',
    'loadscript.bas',
]

#VariantDir('build_luatext', '.')
luatextenv = luaenv.Clone()
luatextenv['VM_DEF'] = 'LUATEXT_VM'
LUAVM_TEST_OBJECTS = [luatextenv.Object (target = targetname(module, 'luat-'), source = module) for module in LUAVM_TEST_MODULES]

LUATEXTVM_TEST = luatextenv.BASEXE (rootdir + 'luavm_test', source = LUAVM_TEST_OBJECTS)
Default (LUATEXTVM_TEST)

env.Alias ('lttest', source = LUATEXTVM_TEST, action =
           ['./luavm_test -o test_result.txt'])
AlwaysBuild ('lttest')
Depends ('lttest', SCRIPTS)


########################################

Help ("""
Options:
  arch=64             64-bit builds (always defaults to 32)

Executables:
  h4vm_test
  luavm_test

Other targets:
  h4test              Run h4vm_test and check output
  lttest              Run luavm_test
""")
