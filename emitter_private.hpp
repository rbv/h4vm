#include "opcodes.h"
#include "ir_h4.hpp"
#include "macros.h"

enum operatorflags {
  //  takesGEN	= 0x1,   //assume always, for now, and see if that holds
  hasDELG	= 0x2,
  hasPUSH	= 0x4,

};

// increment a pointer as if it's a char*
#define INC_PTR(ptr, amount) (*(char**)&(ptr)) += (amount)

#define EMIT_DBG(str, val) DPRINT(("; " str, val));

#define PUSH_OP(ptr, val) EMIT_DBG("OP %s", getopcname(OPADDR(val))) *((OP_TYPE *)(ptr)) = OPADDR(val); INC_PTR(ptr, sizeof(OP_TYPE))
#define PUSH_REG(ptr, val) EMIT_DBG("R " LOCAL_NUM_FMTSTR, val)*((LOCAL_NUM_TYPE *)(ptr)) = val; INC_PTR(ptr, sizeof(LOCAL_NUM_TYPE))
#define PUSH_STKREG(ptr, st, val) EMIT_DBG("STKR " LOCAL_NUM_FMTSTR, val)*((LOCAL_NUM_TYPE *)(ptr)) = val; st->stk_reg_addrs.push_back((char *)(ptr) - (char *)st->code); INC_PTR(ptr, sizeof(LOCAL_NUM_TYPE))
#define PUSH_INT(ptr, val) EMIT_DBG("I %d", val) *((int *)(ptr)) = val; INC_PTR(ptr, sizeof(int))
#define PUSH_SHORT(ptr, val) EMIT_DBG("S %hd", val) *((short *)(ptr)) = val; INC_PTR(ptr, sizeof(short))
#define PUSH_DOUBLE(ptr, val) EMIT_DBG("D %f", val) *((double *)(ptr)) = val; INC_PTR(ptr, sizeof(double))
#define PUSH_PTR(ptr, val) EMIT_DBG("P %p", val) *((void **)(ptr)) = val; INC_PTR(ptr, sizeof(void *))
