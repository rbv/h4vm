#ifndef LOADSCRIPT_H
#define LOADSCRIPT_H

#include "scrconst.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ScriptCommand ScriptCommand;
struct ScriptCommand {
  int kind;
  int id;
  int argc;
  int args[];
};




typedef struct ScriptData ScriptData;

//WARNING: don't add strings to this
struct ScriptData {
  //Script attributes
  int id;               //id number of script  (set to 0 to mark as unused slot)
                        //The id number is negated if this is a stale unreloaded script and
                        //shouldn't be used for new scripts.
  int *ptr;             //pointer to script commands
  int scrformat;        //hsz file format version number
  int headerlen;        //header length in bytes
  int size;             //size of script data, in 4 byte words (in-memory size, not on-disk size)
  int vars;             //local variable (including arguments) count, not including nonlocals
  int nonlocals;        //number of nonlocal variables (sum of locals in ancestor scripts)
  int args;             //number of arguments
  int strtable;         //pointer to string table (offset from start of script data in 4 byte ints)
  int nestdepth;        //Number of scripts/subscripts this is nested in, 0 for scripts
  int parent;           //ID of parent script or 0 if not a subscript

  //Book keeping
  int refcount;         //number of ScriptInst pointing to this data
  int totaluse;         //total number of times this script has been requested since loading
  int lastuse;
  double totaltime;     //time spent in here, in seconds. Used only if SCRIPTPROFILE is defined
  int entered;          //number of times entered. Used only if SCRIPTPROFILE is defined

  ScriptData *next;     //next in linked list, for hashtable
  ScriptData **backptr; //pointer to pointer pointing to this, in script(), or a .next pointer
                        //not your usual double linked list, because head of list is a script() element
};


ScriptData *loadscript(int n, int loaddata);
void freescripts(int mem);

#ifdef __cplusplus
}
#endif

#endif
