/*
  This file is part of the backend-generic script interpreter.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "interpreter.h"

void fatal(const char *fmt, ...) {
  va_list vl;

  va_start(vl, fmt);
  fprintf(stderr, "\nfatal error:\n");
  vfprintf(stderr, fmt, vl);
  fprintf(stderr, "\n");
  va_end(vl);
  exit(1);
}

void scriptout(const char *fmt, ...) {
  va_list vl;
  /*
  FILE *file = outfile;
  if (!file)
    file = stdout;
  */

  va_start(vl, fmt);
  vfprintf(stdout, fmt, vl);
  if (outfile)
    vfprintf(outfile, fmt, vl);
  va_end(vl);
}


/********************************* Terminal **********************************/
// Abstracts text formatting, for printing to terminal, Windows cmd, or
// OHRRPGCE text.
// TODO: switch to using markup tags compatible with the OHR rather than functions.

char *col_bold() {
  return "\e[1m";
}

char *col_reset() {
  return "\e[0m";
}
