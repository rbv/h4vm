#include <stdlib.h>
//#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "interpreter.h"
#include "ir.hpp"
//#include "opcodes.h"
#include "ohrrpgce.h"

class ScriptTranslator {
public:
  ScriptTranslator(ScriptData *scr) : script(scr) {}

  void translate(OpStream *st) { translate_node(NODE(0), st, false); }

protected:
  ScriptData *script;

  ScriptCommand *NODE(int offset) { return (ScriptCommand *)(script->ptr + offset); }
  VirtualRegNum read_variable_num(ScriptCommand *node, OpStream *st);

  void translate_flow_node(ScriptCommand *node, OpStream *st, bool isexpression);
  void translate_math_node(ScriptCommand *node, OpStream *st, bool isexpression);
  void translate_node(ScriptCommand *node, OpStream *st, bool isexpression);
};


void ScriptTranslator::translate_flow_node(ScriptCommand *node, OpStream *st, bool isexpression) {
  assert(!isexpression);
  int i;
  switch (node->id) {
  case flowdo:
  do_like:
    //emit_begindo(st, 1);
    for (i = 0; i < node->argc; i++)
      translate_node(NODE(node->args[i]), st, false);
    break;
  case flowreturn:
    // TODO
    break;
  case flowif:
    if (node->argc != 3)
      fatal("corrupt if node");
    st->ifStart();
    translate_node(NODE(node->args[0]), st, true);
    st->ifCond();
    translate_node(NODE(node->args[1]), st->ifThenStream(), false);
    translate_node(NODE(node->args[2]), st->ifElseStream(), false);
    st->ifEnd();
    break;
  case flowthen:
  case flowelse:
    //note: sadly, HSpeak used to allow freestanding then() and else() blocks, and people used them (eg. SoJ)
    goto do_like;
  case flowfor:
    break;
  case flowwhile:
    break;

  case flowbreak:
    //if (NODE(node->args[0])->kind == tynumber)
      //node->args[0]->id;
    break;
  case flowcontinue:
    break;
  case flowexitreturn:

  case flowexit:
    break;
  case flowswitch:
    break;
  default:
    fatal("invalid flow id %d", node->id);
  }
}

// Translations for math commands which can be converted straight to an operator
struct MathFunc {
  OperatorNum num;
  int argc;
} mathCmdTable[] = {
  {opnumInvalid,   0},	  // mathrandom = 0,
  {opnumInvalid,   0},	  // mathexponent = 1,
  {opnumMod,       2},	  // mathmodulus = 2,
  {opnumDivInt,    2},	  // mathdivide = 3,
  {opnumMult,      2},	  // mathmultiply = 4,
  {opnumSub,       2},	  // mathsubtract = 5,
  {opnumAdd,       2},    // mathadd = 6,
  {opnumInvalid,   0},	  // mathxor = 7,
  {opnumInvalid,   0},	  // mathor = 8,
  {opnumInvalid,   0},	  // mathand = 9,
  {opnumEqual,     2},	  // mathequal = 10,
  {opnumInequal,   2},	  // mathnotequal = 11,
  {opnumInvalid,   0},	  // mathlessthan = 12,
  {opnumInvalid,   0},	  // mathgreaterthan = 13,
  {opnumInvalid,   0},	  // mathlessthanorequalto = 14,
  {opnumInvalid,   0},	  // mathgreaterthanorequalto = 15,
  {opnumInvalid,   0},	  // mathsetvariable = 16,
  {opnumInvalid,   0},	  // mathincrement = 17,
  {opnumInvalid,   0},	  // mathdecrement = 18,
  {opnumInvalid,   0},	  // mathnot = 19,
  {opnumInvalid,   0},	  // mathlogand = 20,
  {opnumInvalid,   0},	  // mathlogor = 21,
  {opnumInvalid,   0},	  // mathlogxor = 22,
};

// Read a integer node which specifies a variable for setvariable, increment, decrement, for loop counter
// variable specifying node. If a local variable, return the id.
// If a global... emit to opstream and return -1?
VirtualRegNum ScriptTranslator::read_variable_num(ScriptCommand *node, OpStream *st) {
  if (node->kind != tynumber)
    fatal("variable specifier is kind %d", node->kind);
  if (node->id >= 0)
    fatal("global variables unimplemented");
  int var_id = -node->id - 1;
  int frame_id = var_id / 256;
  var_id &= 255;
  if (frame_id == 0) {
    // local variable
    if (var_id >= script->vars)
      fatal("out of range local variable number");
    return st->localNum(var_id);
  } else {
    // nonlocal
    fatal("upvals not implemented");
    // This isn't correct...
    return st->upvalNum(var_id, frame_id);
  }
}


// isexpression: if true, want the return value of this expression placed on the stack rather
// than discarding it (e.g. a child of a do())
void ScriptTranslator::translate_math_node(ScriptCommand *node, OpStream *st, bool isexpression) {
  if (node->id < 0 || node->id > mathMAX)
    fatal("illegal math command id %d", node->id);

  MathFunc op = mathCmdTable[node->id];
  if (op.num != opnumInvalid) {
    if (node->argc != op.argc)
      fatal("expected math command %d to have %d args, had %d", node->id, op.argc, node->argc);

    st->opStart(op.num);
    for (int i = 0; i < node->argc; i++)
      translate_node(NODE(node->args[i]), st, true);
    st->op(op.num);
    if (!isexpression)
      st->endStatement();
  } else {
    int var_id;
    switch (node->id) {
    case mathsetvariable:
    case mathincrement:
    case mathdecrement:
      var_id = read_variable_num(NODE(node->args[0]), st);

      if (node->id == mathincrement || node->id == mathdecrement)
	st->pushLocal(var_id);

      translate_node(NODE(node->args[1]), st, true);
      if (node->id == mathincrement)
	st->op(opnumAdd);
      if (node->id == mathdecrement) // TODO
        {}
      if (isexpression)
	st->copyToLocal(var_id);
      else
	st->setLocal(var_id);
      break;
    default:
      fatal("unsupported math command id %d", node->id);
    }
  }
}

/*
void ScriptTranslator::translate_script_node(ScriptCommand *node, OpStream *st, bool isexpression) {
  ScriptData *callee = loadscript(node->id, false);
  if (!callee)
    fatal("Tried to call script id %d which doesn't exist", node->id);
  if (callee->parent) {
    // Calling a subscript
    //assert(callee->parent == our_id);
    //st->
  }
}
*/

// Returns pointer at end of translated node.
// isexpression: if true, want the return value of this expression placed on the stack
void ScriptTranslator::translate_node(ScriptCommand *node, OpStream *st, bool isexpression) {
  switch (node->kind) {
  case tynumber:
    st->pushInt(node->id);
    if (!isexpression)
      st->endStatement();
    break;
  case tyflow:
    translate_flow_node(node, st, isexpression);
    break;
  case tyglobal:
    // TODO
    if (!isexpression)
      st->endStatement();
    break;
  case tylocal:
    st->pushLocal(node->id);
    if (!isexpression)
      st->endStatement();
    break;
  case tymath:
    translate_math_node(node, st, isexpression);
    break;
  case tyfunct:
    st->funcStart(node->id, node->argc, isexpression);
    for (int i = 0; i < node->argc; i++)
      translate_node(NODE(node->args[i]), st, true);
    st->func(node->id, node->argc, isexpression);
    if (!isexpression)
      st->endStatement();
    break;
  case tyscript:
    //printf("call script %d, %d args\n", node->id, node->argc);
    st->callStart(node->argc, isexpression);
    st->pushScript(node->id);
    for (int i = 0; i < node->argc; i++)
      translate_node(NODE(node->args[i]), st, true);
    st->call(node->argc, isexpression);
    if (!isexpression)
      st->endStatement();
    //translate_script_node(node, st, isexpression);
    break;
  default:
    fatal("invalid node kind %d", node->kind);
  }
}

CompiledScript *converthsz(ScriptData *scr) {
  // args is 999 in ancient script format which didn't record actual number
  if (scr->args > scr->vars)
    scr->args = scr->vars;
  OpStream stream(scr->id, scr->args, scr->vars - scr->args, scr->nonlocals);
  printf("Translating script %d, (%d bytes commands + %d bytes strings)...\n", scr->id,
	 (scr->strtable ? scr->strtable : scr->size) * 4, (scr->strtable ? scr->size - scr->strtable : 0) * 4);
  ScriptTranslator translator(scr);
  translator.translate(&stream);
  CompiledScript *p = stream.emit();
  return p;
}
