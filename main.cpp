/*
  Interpreter test code

 */

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "interpreter.h"
#include "loadscript.h"
#include "opcodes.h"
//#include "emit.h"
#include "ir_h4.hpp"
#include "ohrrpgce.h"

FILE *outfile;

// This testcase can definitely not be supported by ir_luatext.
// In fact, can't even make it use the API properly, which
// requires opStart, funcStart and doesn't allow 'if' in an expression.
// Also doesn't support copyToLocal.
void emit_test_script() {
  OpStream stream(0, 0, 1, 0);  // 1 local
  // temp = local0 = -35;
  stream.pushInt(-35);
  stream.copyToLocal(0);
  int temp;
  temp = stream.newTemp();
  stream.setLocal(temp);
  // (future) func99(
  stream.funcStart(99, 2, false);
  // 77 + temp, 10
  stream.opStart(opnumAdd);
  stream.pushInt(77);
  stream.pushLocal(temp);
  stream.freeTemp(temp);
  stream.op(opnumAdd);
    // Can't put stream.opStart(opnumAdd) here!
  stream.pushInt(10);
  // if local0 then + 100 end
  stream.ifStart();
  stream.pushLocal(stream.localNum(0));
  stream.ifCond();
  OpStream *thenStream = stream.ifThenStream();
   thenStream->pushInt(100);
   thenStream->op(opnumAdd);
   /*
  stream.elseBlock();
   stream.pushInt(20);
   */
  stream.ifEnd();
  // func99(..., ... + 1)
  stream.pushInt(1);
  stream.op(opnumAdd);
  stream.func(99, 2, false);
  stream.endStatement();
  // end
  CompiledScript *scr = stream.emit();
  printf("\nemitted, running\n");
  int *locals = (int*)calloc(128, 1);
  scr->run(locals);
  free(locals);
  delete scr;
  printf("done.\n");
}

#define OP(code) (code - 900000)

intptr_t testprogram[] =
{
  OP(opSET_SP), 4,
  OP(opMOV_INT), 0, -1,
  OP(opMOV_INT), 1, 10,
  OP(opMOV_INT), 2, 1,
  OP(opMOV_INT), 3, 0,
  OP(opREG), 1,
  /*lab1*/
  OP(opINC_INT_INT), 3,
  OP(opFOR_INC_GTEQ_INT), 1, 0, 2, 16*4 /*lab1*/,
  OP(opPRINT_VAR), 3,
  OP(opINT), 5,
  OP(opPRINT_INT), 555,
  OP(opADD_INT_INT), 0,
  OP(opJUMPNZ), 27*4,
  OP(opNEW_STRING), 0, (intptr_t)"hello!", 7,
  OP(opSET_VAR), 10001, 10000, /* fixup: Need fixup: &locals[0], &locals[1] */
  OP(opSET_REG), 3,
  OP(opINT), 100,
  OP(opSET_REG), 0,
  OP(opPRINT_STATE),
  OP(opSTART_TIMER),
  OP(opDO_INT), 5,
  OP(opLOOP),
  OP(opSTOP_TIMER),
  OP(opINT), 13,
  OP(opPUSH),
  OP(opPRINT_VAR), 0,
  OP(opDOUBLE), (intptr_t)0x9ba5e354, (intptr_t)0x400920c4,  /*  3.141 */
  OP(opSET_REG), 2,
  OP(opPRINT_STATE),
  OP(opADD_ANY_ANY), 0,
  OP(opPUSH),
  OP(opINT), 54,
  OP(opPUSH),
  OP(opPRINT_STATE),
  OP(opCALL_BUILTIN_STACK), 131100,
  OP(opDEL), 5,
  OP(opREG), 2,
  OP(opSET_REG), 1,
  OP(opPRINT_STATE),
  OP(opDEL_REGS), 5,
  OP(opEXIT)
};

#define ALEN(array) (sizeof(array) / sizeof(array[0]))

// Translate the testprogram template array to an opcode array
void **get_testprogram(int *locals) {
  static void *ret[ALEN(testprogram)];
  for (unsigned int i = 0; i < ALEN(testprogram); i++) {
    if (testprogram[i] == 10000)
      ret[i] = &locals[0];
    else if (testprogram[i] == 10001)
      ret[i] = &locals[1];
    else if (testprogram[i] >= -900000 && testprogram[i] < -899000)
      ret[i] = OPADDR(testprogram[i] + 900000);
    else
      ret[i] = (void*)testprogram[i];
  }
  printf("disassembling:\n");
  disassembleBC(ret, ret + ALEN(testprogram));
  return ret;
}

void test_converthsz() {
  scriptout("Converting and running script 1.hsz...\n");
  ScriptData *scr = loadscript(1, true);
  if (!scr)
    fatal("failed to loadscript");
  CompiledScript *script = converthsz(scr);
  printf("\n\nemitted, running\n");
  int *locals = (int*)calloc(128, 1);
  if (!script->run(locals))
    printf("%sScript %d threw error%s\n", col_bold(), id, col_reset());
  delete scr;
  free(locals);
  printf("done.\n");
}

int functions_callback(int id, int argc, int *argv, int typed_mask) {
  int i;
  scriptout("Called command %d, with %d args: \n", id, argc);
  for (i = 0; i < argc; i++) {
    scriptout(" <%d> = ", i);
    if (typed_mask & (1 << i))
      printvariable(argv[i]);
    else
      scriptout("%d, ", argv[i]);
  }
  scriptout("\n");
  return 33779;
}

int main(int argc, char **argv) {
  int i;
  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-o")) {
      if (i + 1 >= argc)
	fatal("expected filename after -o");
      i++;
      outfile = fopen(argv[i], "w");
    } else
      fatal("unknown parameter %s", argv[i]);
  }

  init_interpreter();

  scriptout("Testing built-in test op-stream...\n");
  emit_test_script();
  scriptout("\n");

  scriptout("Testing built-in pre-compiled script...\n");
  int *locals = (int*)calloc(128, 1);
  bcloop(0, get_testprogram(locals), locals);
  free(locals);
//  printf("CPU loops : %"PRIu64"\n", stamp_t2 - stamp_t1); // bloody MS doesn't support %llu, and gcc throws an incorrect warning
  printf("CPU loops : %llu\n", stamp_t2 - stamp_t1);
  scriptout("\n");

  /**********  test hsz converter ********/
  printf("\nrestart\n");
  test_converthsz();

  del_data_tables();

  return 0;
}
