
#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <stdio.h>

#include "macros.h"
#include "util.h"


#ifdef __cplusplus
extern "C" {
#endif

/* OBSOLETE
// 10CCXXXX XXXXXXXX XXXXXXXX XXXXXXXX

//CC=00: string
//CC=01: double
//CC=10: array
//CC=11: user type
*/

 // IDEAS
 //** As above, with global hash_set containing pointers of untyped low ints
 //** As above, with a bit array for each array of typed vars - required pointer to var array to use var
 //** As above, interspersing each array with a bits itn every 128 bytes. Force 128 byte alignment?
 //** Add a low_int type stored in global array
 //** Store low_ints as doulbes

 //** all objects may be either in encoded or decoded form in a 32bit var:
 //   *ints may be plain or encoded - unfortunately impossible to distinguish encoded from low ints
 //   *other types may be pointers (always < 2^31 I think?) or encoded

/* THE REAL ENCODING */
// 10CCCRXX XXXXXXXX XXXXXXXX XXXXXXXX

//CCC=000: low int
//CCC=001: string
//CCC=010: double
//CCC=011: array
//CCC=100: user type
//R: on if counts towards refcount, always 0 for low_ints

enum {
  T_LOW_INT	= 0,
  T_STRING	= 1,
  T_DOUBLE	= 2,
  T_ARRAY	= 3,
  T_UDT		= 4
};

#define FLGBASE     0x80000000
#define FLGREFC     0x04000000
#define FLG_LOW_INT (FLGBASE + (T_LOW_INT << 27))
#define FLG_STRING  (FLGBASE + (T_STRING << 27) + FLGREFC) //0x8c000000
#define FLG_ARRAY   (FLGBASE + (T_ARRAY << 27) + FLGREFC)  //0x9c000000
#define FLG_DOUBLE  (FLGBASE + (T_DOUBLE << 27) + FLGREFC) //0x90000000

#define VAR_TYPED(x) ((signed int)(x) < (signed int)0xc0000000)
#define VAR_TYPE(x) (((int)(x) >> 27) & 0x7)
#define VAR_T_ID(x) ((int)(x) & 0x03ffffff)
#define VAR_LOW_INT(x) ((signed int)(x) < (signed int)0x88000000) 
#define VAR_INT(x) (VAR_TYPED(x) ? VAR_LOW_INT(x) ? x = low_ints.p[x], 1 : 0 : 1)  // holy cow, figure that one out!
#define TYPE_REFCED(x) (((int)(x) >> 26) & 1)
#define VAR_FLAGS(x) ((x) & 0xfc000000)

//#define VAR_INT(x) (VAR_TYPED(x) ? (x = low_ints.p[VAR_T_ID(x)], 1) : 0)
//#define VAR_INT(x) (VAR_TYPED(x) && VAR_T_ID(x) < low_ints.size ? (x = low_ints.p[VAR_T_ID(x)], 1) : 0)


void fatal(const char *fmt, ...) format_chk(1) _noreturn;

// Interpreter output used for tests or printed to any kind of console/REPL
void scriptout(const char *fmt, ...) format_chk(1);
char *col_bold();
char *col_reset();


extern FILE *outfile;

struct data_table {
  int *p;
  int num;
  int size;
  
  int key_start;
  int elmt_size; // in 4-byte words. required to be a power of 2

  void (*delete_entry)(void *);
};
extern struct data_table tables[5];
/*
union {
  struct data_table tables[5];
 low_ints;
extern struct data_table low_ints;
*/


struct string_var {
  int refc;
  char *ptr;
  int size;
  int dummy;
};

struct double_var {
  int refc;
  int dummy;
  double d;
};

//???
struct array_var {
  enum array_type {vars, ints, floats, strings, typed, multid} type;
  int *data;
  int refcount;
  char elmt_bytes;
};


int new_table_entry(struct data_table *tbl, int **entry);
int ref(int gen);
void deref(int var);
int init_data_tables();
void del_data_tables();
void print_tables();

void bcloop(int init, void **inst, int *locals);

void init_interpreter();
void shutdown_interpreter();

void disassembleBC(void *code, void *code_end);

char *valuetoa(int);
void printvariable(int);

int to_doubles_or_ints(int *arg1, int *arg2, double *d1, double *d2);

int functions_callback(int id, int argc, int *argv, int typed_mask);

extern void **opcodes;

#define LOCAL_NUM_TYPE int //short
#define OP_TYPE void *
#define LOCAL_NUM_FMTSTR "%d"

extern unsigned long long stamp_t1, stamp_t2;

struct IROperator;
struct emit_state;

#ifdef __cplusplus
}
#endif

#endif
