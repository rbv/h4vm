#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
// for debugging
#include <stdio.h>

#include "interpreter.h"

void array_new(struct array *a) {
  a->allocd = 8;
  a->p = malloc(a->allocd * sizeof(a->p[0]));
  a->end = 0;
}

void array_push(struct array *a, int val) {
  a->end += 1;
  if (a->end > a->allocd) {
    a->allocd *= 2;
    a->p = realloc(a->p, a->allocd * sizeof(a->p[0]));
    if (!a->p)
      fatal("array_push: out of memory");
  }
  a->p[a->end - 1] = val;
}


/*
static void regs_resize(struct regarray *array, int i) {
  if (i > array->max) {
    array->max = i;
    if (i > array->allocd) {
      array->allocd = i * 2;
      array->t = realloc(array->t, array->allocd * sizeof(array->t[0]));
    }
  }
}
*/

/******************************** Memory Pool ********************************/

mempool_t *mempool_new(int blksize) {
  assert((unsigned int)blksize > sizeof(struct memchunk) + 4);
  // check blksize is a power of two
  assert((blksize & (blksize - 1)) == 0);
  mempool_t *pool = malloc(sizeof(mempool_t));
  pool->alignment = blksize;
  pool->blocksize = blksize;
  // seven blocks at a time, plus enough to enable alignment.
  pool->chunksize = blksize * 8 - 4;
  pool->freelist = NULL;
  pool->chunklist = NULL;
  return pool;
}

void *mempool_allocblk(mempool_t *pool) {
  if (pool->freelist) {
    void *temp = pool->freelist;
    pool->freelist = *pool->freelist;
    return temp;
  }

  // grab a new chunk
  void *chunk = malloc(pool->chunksize);
  void *blk = (void *)((intptr_t)(chunk + pool->alignment - 1) & ~(pool->alignment - 1));
  struct memchunk *tag;
  if (blk - chunk >= (int)sizeof(struct memchunk))
    tag = chunk;
  else
    tag = chunk + pool->chunksize - sizeof(struct memchunk);
  tag->base = chunk;
  tag->next = pool->chunklist;
  pool->chunklist = tag;

  // add all new blocks to freelist
  for ( ;blk <= chunk + pool->chunksize - pool->blocksize; blk += pool->blocksize) {
    *(void ***)blk = pool->freelist;
    pool->freelist = blk;
  }
  assert((void *)tag >= blk || (void *)tag == chunk);
  return(mempool_allocblk(pool));
}

void mempool_freeblk(mempool_t *pool, void *blk) {
  *(void **)blk = pool->freelist;
  pool->freelist = blk;
}

