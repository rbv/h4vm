//--SCRIPT STATEMENT TYPES---
enum {
	tystop = 0,    //0 terminate script (not really legal)
	tynumber = 1,  //1 literal number
	tyflow = 2,    //2 flow control
	tyglobal = 3,  //3 global variable
	tylocal = 4,   //4 local variable
	tymath = 5,    //5 math function
	tyfunct = 6,   //6 function call
	tyscript = 7,  //7 script call
};
//--FLOW CONTROL TYPES---
enum {
	flowdo = 0,
	flowreturn = 3,
	flowif = 4,
	flowthen = 5,
	flowelse = 6,
	flowfor = 7,
	flowwhile = 10,
	flowbreak = 11,
	flowcontinue = 12,
	flowexit = 13,
	flowexitreturn = 14,
	flowswitch = 15,
};
//--SCRIPT TRIGGER TYPES--
enum {
	plottrigger = 1,
};

enum {
  mathrandom = 0,
  mathexponent = 1,
  mathmodulus = 2,
  mathdivide = 3,
  mathmultiply = 4,
  mathsubtract = 5,
  mathadd = 6,
  mathxor = 7,
  mathor = 8,
  mathand = 9,
  mathequal = 10,
  mathnotequal = 11,
  mathlessthan = 12,
  mathgreaterthan = 13,
  mathlessthanorequalto = 14,
  mathgreaterthanorequalto = 15,
  mathsetvariable = 16,
  mathincrement = 17,
  mathdecrement = 18,
  mathnot = 19,
  mathlogand = 20,
  mathlogor = 21,
  mathlogxor = 22,
  mathMAX = 22
};
