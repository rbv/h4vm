#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "interpreter.h"
#include "ir_luatext.hpp"
#include "ohrrpgce.h"

FILE *outfile;

int tests_run = 0, tests_failed = 0;

int functions_callback(int id, int argc, int *argv, int typed_mask) {
  int i;
  scriptout("Called command %d, with %d args: ", id, argc);
  for (i = 0; i < argc; i++) {
    scriptout(" <%d> = ", i);
    if (typed_mask & (1 << i))
      printvariable(argv[i]);
    else
      scriptout("%d, ", argv[i]);
  }
  scriptout("\n");

  if (id == 1000) {   // _assert
    tests_run++;
    if (argc != 1 || !argv[0]) {
      scriptout("%s_assert() failure!%s\n", col_bold(), col_reset());
      tests_failed++;
    }
  } else if (id == 1001) {  // _asserteq
    tests_run++;
    if (argc != 2 || argv[0] != argv[1]) {
      scriptout("%s_asserteq(%d, %d) failure!%s\n", col_bold(), argv[0], argv[1], col_reset());
      tests_failed++;
    }
  }

  return 33779;
}

void test_converthsz(int id) {
  scriptout("\n---------\nConverting and running script %d.hsz...\n", id);
  ScriptData *scr = loadscript(id, true);
  if (!scr)
    fatal("failed to loadscript");
  CompiledScript *script = converthsz(scr);
  printf("emitted, running\n");
  int *locals = (int*)calloc(128, 1);
  bool result = script->run(locals);
  delete scr;
  free(locals);
  if (result)
    printf("Script %d finished.\n", id);
  else
    printf("%sScript %d threw error%s\n", col_bold(), id, col_reset());
}

int main(int argc, char **argv) {
  int i;
  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-o")) {
      if (i + 1 >= argc)
	fatal("expected filename after -o");
      i++;
      outfile = fopen(argv[i], "w");
    } else
      fatal("unknown parameter %s", argv[i]);
  }

  init_interpreter();

  test_converthsz(2);
  test_converthsz(3);
  test_converthsz(4);
  //test_converthsz(5);
  test_converthsz(6);

  printf("\n%s%d out of %d asserts failed.%s\n", col_bold(), tests_failed, tests_run, col_reset());

  shutdown_interpreter();

  return 0;
}
