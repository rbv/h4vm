#include <stdlib.h>
#include <stdio.h>

#include "interpreter.h"

struct data_table tables[5];

/*
// perform a quadratic search (cache friendly and clump free) for an unused low_ints entry and return encoded variable
int add_low_int(int x) {
  low_ints.num++;
  if (low_ints.num + low_ints.num/2 >= low_ints.size) {
    low_ints.size *= 2;
    if (!(low_ints.p = realloc(low_ints.p, low_ints.size * sizeof(int))))
	  OUT_OF_MEM;
  }
  static int start = 0;
  int probe = start & (low_ints.size - 1);
  //start += 1;
  int inc = 1;
  //this will search every entry in the table before repeating, see eg. http://www.chilton-computing.org.uk/acl/literature/reports/p012.htm
  while (low_ints.p[probe]) {
    probe += inc++;
    probe &= low_ints.size - 1;
  }
  start = probe + 1;
  low_ints.p[probe] = x;
  return 0x80000000 | probe;
}
*/

//1101001000100001000001000000100000001
//001101001000100001000001000000100000001

// perform a quadratic search (cache friendly and clump free) for an unused low_ints entry and return encoded variable
// returns offset in ints of entry from start of table, and puts its address in *entry
int new_table_entry(struct data_table *tbl, int **entry) {
  tbl->num++;
  if (tbl->num + tbl->num/2 >= tbl->size) {
    tbl->size *= 2;
    if (!(tbl->p = realloc(tbl->p, tbl->size * tbl->elmt_size * sizeof(int))))
      fatal("OUT OF MEM");
  }
  int mask = (tbl->size * tbl->elmt_size) - 1;
  int probe = tbl->key_start & mask;
  //start += 1;
  int inc = tbl->elmt_size;
  //this will search every entry in the table before repeating, see eg. http://www.chilton-computing.org.uk/acl/literature/reports/p012.htm
  while (tbl->p[probe]) {
    probe += inc;
    inc += tbl->elmt_size;
    probe &= mask;
  }
  if (entry)
    *entry = &tbl->p[probe];
  tbl->key_start = probe + tbl->elmt_size;
  return probe;
}

/*
// takes an encoded variable and removes from low_ints
void del_low_int(int var) {
  low_ints[VAR_T_ID(var)] = 0;
  low_ints.num--;
}
*/

int init_table(struct data_table *tbl, int size, int elmt_size, void (*del_mthd)(void*)) {
  tbl->size = size;
  tbl->num = 0;
  tbl->p = calloc(size * elmt_size * sizeof(int), 1);
  tbl->key_start = 0;
  tbl->elmt_size = elmt_size;
  tbl->delete_entry = del_mthd;
  return 0;
}

void del_table(struct data_table *tbl) {
  int i = 0;
  if (tbl->delete_entry)
    while (i < tbl->size) {
      tbl->delete_entry(&tbl->p[i]);
      i += tbl->elmt_size;
    }
  free(tbl->p);
}

//#define FREE_VAR(var) if (VAR_TYPED(var)) { if (VAR_LOW_INT(var)) del_low_int(var); else deref(var); }

void deref(int var) {

  if (VAR_TYPED(var)) {
    struct data_table *tbl = &tables[VAR_TYPE(var)];
    int *tempp = &tbl->p[VAR_T_ID(var)];
    if (TYPE_REFCED(var)) {
      /* decrement reference count */
      if (--tempp[0] == 0)
	tbl->num--;
    } else {
      /* delete data */
      tempp[0] = 0;
      tbl->num--;
    }
  }

}

int ref(int gen) {
  if (VAR_TYPED(gen)) {
    struct data_table *tbl = &tables[VAR_TYPE(gen)];
    int *tempp = &tbl->p[VAR_T_ID(gen)];
    if (TYPE_REFCED(gen)) {
      /* increment reference count */
      tempp[0]++;
    } else {
      /* duplicate data */
      //scriptout("duplicating %s\n", valuetoa(gen));
      int *tempp2;
      int ret = VAR_FLAGS(gen) | new_table_entry(tbl, &tempp2);
      int i;
      for (i = 0; i < tables[VAR_TYPE(gen)].elmt_size; i++)
	tempp2[i] = tempp[i];
      return ret;
    }
  }
  return gen;
  /*
    if (VAR_LOW_INT(temp)) {
    temp2 = new_table_entry(&low_ints);
    low_ints.p[temp2] = low_ints.p[temp];
    *(int *)pc[1] = 0x80000000 & temp2;
    } else if (TYPE_REFCED(x))
  */
}

// AKA double_or_nothing
// arg1 and arg2 point to two encoded variables
// If either is not an int or double, throw an error
// If either is a double, convert/retrieve both to doubles and place
// in *d1 and *d2, and return true
// Otherwise, decode both to ints and place in *arg1 and *arg2 and return false.
int to_doubles_or_ints(int *arg1, int *arg2, double *d1, double *d2) {
  int floatret = 0;
  if (VAR_TYPED(*arg1)) {
    void *data = &tables[VAR_TYPE(*arg1)].p[VAR_T_ID(*arg1)];
    if (VAR_TYPE(*arg1) == T_LOW_INT) {
      *arg1 = *(int *)data;
    } else if (VAR_TYPE(*arg1) == T_DOUBLE) {
      floatret = 1;
      *d1 = ((struct double_var *)data)->d;
    } else
      fatal("to_doubles_or_ints: bad arg1 type: %s", valuetoa(*arg1));
  }

  if (VAR_TYPED(*arg2)) {
    void *data = &tables[VAR_TYPE(*arg2)].p[VAR_T_ID(*arg2)];
    if (VAR_TYPE(*arg2) == T_LOW_INT) {
      *arg2 = *(int *)data;
    } else if (VAR_TYPE(*arg2) == T_DOUBLE) {
      *d2 = ((struct double_var *)data)->d;
      if (!floatret)
	*d1 = (double)*arg1;
      return 1;
    } else
      fatal("to_doubles_or_ints: bad *arg2 type: %s", valuetoa(*arg2));
  }
  if (floatret)
    *d2 = (double)*arg2;
  return floatret;
}

// encode a 32 bit int.
// goto ENCODE_INT instead
//int encodeint(int arg) {
//}

void delete_string(void *p) {
  free(((struct string_var *)p)->ptr);
  ((struct string_var *)p)->ptr = NULL;
}


void print_tables() {
  int i;
  scriptout("String table (%d out of %d entries):\n", tables[T_STRING].num, tables[T_STRING].size);
  for (i = 0; i < tables[T_STRING].size; i += tables[T_STRING].elmt_size) {
    struct string_var *p = (struct string_var *)&tables[T_STRING].p[i];
    if (p->refc)
      scriptout("%d: %d ref %d*\"%s\"\n", i, p->refc, p->size, p->ptr);
  }

  scriptout("Double table (%d out of %d entries):\n", tables[T_DOUBLE].num, tables[T_DOUBLE].size);
  for (i = 0; i < tables[T_DOUBLE].size; i += tables[T_DOUBLE].elmt_size) {
    struct double_var *p = (struct double_var *)&tables[T_DOUBLE].p[i];
    if (p->refc)
      scriptout("%d: %d ref %f\n", i, p->refc, p->d);
  }
}

char *valuetoa(int var) {
  static char buf[64];
  int len;
  len = sprintf(buf, "[0x%08x]:", var);
  if (VAR_TYPED(var)) {
    switch (VAR_TYPE(var)) {
    case T_LOW_INT:
      snprintf(buf + len, 64 - len, "lowint %d", tables[T_LOW_INT].p[var]); //tricky pointer math again
      break;
    case T_STRING:
      snprintf(buf + len, 64 - len, "\"%s\"", ((struct string_var *)(tables[T_STRING].p + VAR_T_ID(var)))->ptr);
      break;
    case T_DOUBLE:
      snprintf(buf + len, 64 - len, "%f",  ((struct double_var *)(tables[T_DOUBLE].p + VAR_T_ID(var)))->d);
      break;
    default:
      scriptout("unknown variable type!\n");
      fatal("printvariable: unknown variable type!");
    }
  } else
    snprintf(buf + len, 64 - len, "%d", var);
  buf[63] = '\0';
  return buf;
}

void printvariable(int var) {
  scriptout("%s\n", valuetoa(var));
}

int init_data_tables() {
  init_table(&tables[T_LOW_INT], 64, 1, NULL);
  init_table(&tables[T_STRING], 64, 4, &delete_string);
  //strings 0 to 31 special
  init_table(&tables[T_ARRAY], 32, 4, NULL);
  //special arrays
  
  init_table(&tables[T_DOUBLE], 32, 4, NULL);

  return 0;
}

void del_data_tables() {
  del_table(&tables[T_LOW_INT]);
  del_table(&tables[T_STRING]);
  del_table(&tables[T_ARRAY]);
  del_table(&tables[T_DOUBLE]);

  //  free(low_ints.p);
}

