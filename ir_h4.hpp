
#ifndef IR_H4_HPP
#define IR_H4_HPP

#include <assert.h>
#include <iterator>
#include <vector>
#include <string>

#include "util.h"
#include "interpreter.h"
#include "opcodes.h"
#include "ir.hpp"

#ifndef NDEBUG
#define DEBUG_OPSTREAM
#endif


// A compiled script
class H4CompiledScript : public CompiledScript {
public:
  ~H4CompiledScript();

  // (Placeholder only) True if no error occurred
  bool run(int *locals);
  
private:
  H4CompiledScript(void **bytecode_) : bytecode(bytecode_) {}
  void **bytecode;

  // H4CompiledScript can only be constructed by calling EmitState.emitCode()
  friend class EmitState;
};

//typedef H4CompiledScript CompiledScript;

struct IROperator {
  const char *name;
  int flags;   // combination of enum operatorflags values
  int argc;
  enum op_id op_any;    // Boxed arguments
  enum op_id op_int;    // Integer arguments
  enum op_id op_encint; // Boxed integers?
};

// Types of registers specified as operands to instructions (OpCodes)
// VSTACK - virtual stack - refers to an item on the stack seen by code
// emitting to the IR API.
// VTEMP - a virtual temporary variable, as allocated by H4OpStream::newtemp/sop_varbegin
// STACK - portion of the real registers treated like a stack
enum OperandTy { regtyNONE, regtyVSTACK, regtyVTEMP, regtyREAL, regtySTACK, regtyGEN, regty_END };
#ifdef REGTY_NAMES
const char *regty_names[] = { "NONE", "vstack", "vtemp", "real", "stack", "gen" };
#endif

class Operand {
public:
  // num defaults to 0, for regtyGEN
  Operand(OperandTy ty = regtyNONE, int num = 0) : regType(ty), regNum(num) {}

  void set(OperandTy ty, int num = 0) {
    regType = ty;
    regNum = num;
  }

  OperandTy type() const { return (OperandTy)regType; }

  int num() const { return regNum; }
  std::string toString() const;

  bool operator==(const Operand &rhs) const {
    return regType == rhs.regType && regNum == rhs.regNum;
  }
  bool operator!=(const Operand &rhs) const { return !(*this == rhs); }

private:
  unsigned int regType : 3;
  int regNum : 29;
};

// Passed around as an int
class VirtualRegister {
public:
  VirtualRegister(int r, bool istemp = false) : reg(r | (VTEMP_FLAG * istemp)) {}
  operator int() { return reg; }

  enum { VTEMP_FLAG = 0x10000 };

  // Create an Operand from a virtual register number returned from H4OpStream::newtemp
  Operand toOperand() {
    if (reg & VTEMP_FLAG) {
      return Operand(regtyVTEMP, reg - VTEMP_FLAG);
    } else {
      // This is a local variable
      return Operand(regtyREAL, reg);
    }
  }
  int toVTemp() {
    assert(reg & VTEMP_FLAG);
    return reg - VTEMP_FLAG;
  }

private:
  int reg;
};


class H4OpStream;

struct _H4OpStreamItem {
  // For inheritance by OpCode.
  // These members are not initialised in the constructor but in
  // H4OpStream::alloc(), called from operator new.

  unsigned char size, prevsize;

  void *operator new(size_t sz, H4OpStream *s, size_t alignment);

  // FIXME: this is a dirty workaround to try and force
  // (void *)opCode == (void *)(_H4OpStreamItem *)opCode
  virtual ~_H4OpStreamItem() {}
};

class OpCode;

// Not sure whether I implemented all bidirectional iterator requirements
class H4OpStreamIterator
  : public std::iterator<std::bidirectional_iterator_tag, OpCode> {
public:
  H4OpStreamIterator() {}
  H4OpStreamIterator(OpCode *p) : ptr((char *)p) {}

  OpCode &operator*() { return *(OpCode *)ptr; }
  OpCode *operator->() { return (OpCode *)ptr; }
  operator OpCode*() { return &**this; }

  H4OpStreamIterator operator++() { ptr += item()->size; return *this; }
  H4OpStreamIterator operator++(int) {
    H4OpStreamIterator ret((OpCode *)ptr);
    ptr += item()->size;
    return ret;
  }
  H4OpStreamIterator operator--() { ptr -= item()->prevsize; return *this; }
  H4OpStreamIterator operator--(int) {
    H4OpStreamIterator ret((OpCode *)ptr);
    ptr -= item()->prevsize;
    return ret;
  }
  bool operator==(H4OpStreamIterator &rhs) { return ptr == rhs.ptr; }

private:
  _H4OpStreamItem *item() { return (_H4OpStreamItem *)ptr; }

  char *ptr;
};

class H4OpStream : public BaseOpStream {
  friend struct _H4OpStreamItem;
public:
  typedef H4OpStreamIterator iterator;

  // Set number of arguments and locals
  H4OpStream(int id_, int args_, int locals_, int upvals_, H4OpStream *parent_ = NULL);
  ~H4OpStream();

  void print(int indent = 0);
  CompiledScript *emit();
  int numTemps() { return *tempCounter; }
  int calcMemUsage();

  // Return the register number for the i-th...
  VirtualRegNum localNum(int which);
  VirtualRegNum argNum(int which);
  VirtualRegNum upvalNum(int levelsUp, int which);

  // Allocate a temporary local variable. Returns a local number (casts to int), which can be
  // passed to pushlocal(), etc. Strictly, freetemp is optional.
  VirtualRegNum newTemp();
  void freeTemp(VirtualRegNum which);

  void pushInt(int i);
  void pushDouble(double d);
  void pushStr(const char *str);
  // Push a 'virtual' register. An int >= 0 may be passed to specify a local
  // variable, otherwise a value returned from newtemp
  void pushLocal(VirtualRegNum which);
  // Apply an operator to the top stack contents and push result. Variable arity
  void op(OperatorNum op);
  // Call opStart before pushing/computing the arguments for an operator
  void opStart(OperatorNum op);
  // Call funcStart before pushing/computing the arguments, and func afterwards
  void funcStart(int id, int argc, bool returnval = true);
  void func(int id, int argc, bool returnval = true);


  // Push a script function onto the stack. Can be a subscript.
  void pushScript(int id);

  // Call callStart(), push the function object followed by the arguments,
  // then call call() afterwards.
  void callStart(int argc, bool returnval = true);
  void call(int argc, bool returnval = true);


  // This is required after an expression which we want to discard the
  // result of (pops top of stack). Should not be called after assignment
  // or flow control.
  void endStatement();
  // setlocal pops stack top, copytolocal preserves it
  void setLocal(VirtualRegNum which);
  void copyToLocal(VirtualRegNum which);

  void ifStart();  // Call before starting condition expression
  void ifCond();   // Call after condition is pushed
  H4OpStream *ifThenStream(); // Stream to which to push 'then' block
  H4OpStream *ifElseStream(); // Stream to which to push 'else' block
  void ifEnd();    // Call after then and else are translated

  iterator begin() { return iterator((OpCode *)&*stream.begin()); }
  // Note: the end() iterator may not be decremented. That is probably not right
  iterator end() { return iterator((OpCode *)&*stream.end()); }
  OpCode &back() { assert(length); return *last; }

  unsigned int size() const { return length; }

private:
  // _H4OpStreamItem::operator new calls alloc(), then the constructor is invoked
  // on the allocated object, and then it is passed to append (which currently
  // only has debugging purposes)
  void *alloc(size_t sz, size_t alignment);

  // append() may only be passed an derived OpCode pointer created with 'new(H4OpStream &) SomeOpCode(...)'
  // Invalidates iterators.
  void append(OpCode *code);

  void pop(); //discard top of stack

  // This H4OpStream may a child of an OpCode which is part of another H4OpStream.
  H4OpStream *parent;

  // This is set when a stream may no longer be written to by the H4OpStream
  // interface. The emitter may still modify it.
  bool closed;

  std::vector<char> stream;
  H4OpStreamIterator last;
  int length;

  int id;  // external id
  // Number of:
  int args, locals, upvals;

  // The same as VmModel's: used in order to absolutely number stack registers
  int stacksz;

  // The number of temporary variables used so far. Each gets a unique number
  int *tempCounter;
};

inline
void *_H4OpStreamItem::operator new(size_t sz, H4OpStream *s, size_t alignment) {
  return s->alloc(sz, alignment);
}

typedef H4OpStream OpStream;
//class OpStream : public H4OpStream {};

#endif
