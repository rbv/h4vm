;Type "alt-x load-file <RET> opcodelists.el" to load
;of the file; and then "alt-x build-opcode-lists"


(defun build-opcode-lists ()
  "Process every OPCODE in loop.c to update opcode lists throughout the source"
  (interactive)
  (save-excursion
    (let* ((output-ops '(("loop.c" "opcodes[] = {\n    /*opNIL*/NULL,\n" "  }" "    &&%s,\n" (repstr))
			 ("opcodes.h" "_op_id {_opNIL, " "}" "_op%s, " (repstr))
			 ("opcodes.h" "op_id {opNIL, " "}" "op%s, " (repstr))
			 ("opcodes.h" "STARTOPS\n" "//ENDOPS" "#define op%s\t\tOPADDR(_op%s)\n" (repstr repstr))
			 ("opcodes.h" "STARTOPDOCS\n" "//ENDOPDOCS" "  {\"%s\", \"%s\", \"%s\"},\n" (repstr argstr docstr))))
					;delete the existing opcode lists
					;and build a list of markers to insert positions
	   (markers (mapcar
		     (lambda (x)
                                        ;x = (filename startpattern endpattern ...)
		       (find-file (nth 0 x)) 
		       (goto-char (point-min))
		       (let (m)
			 (kill-region 
			  (search-forward (nth 1 x) nil nil)
			  (progn (setq m (point-marker))
				 (search-forward (nth 2 x) nil nil)
				 (match-beginning 0)))
			 m))
		     output-ops)))
      (set-buffer "loop.c")
      (goto-char (point-min))
      (while (re-search-forward "^[ \t]*OPCODE\\([_A-Z]*\\)(\\([_A-Z]*\\)) //\\([^)]*)\\):? *\\([^\n]*\\)$" nil t)
	(let ((prefixes (append '(("" ""))
				(if (equal (match-string 1) "DEL")
				    '(("DELG_" "delete(gen); ")))))
	      (baseopname (match-string 2))
	      (argstr (match-string 3))
	      (basedocstr (match-string 4)))
	  (dolist (prefixspec prefixes)
	    (dotimes (i (length output-ops))
	      (let* ((m (nth i markers))
		     (fmtstr (nth 3 (nth i output-ops)))
		     (repstr (concat (nth 0 prefixspec) baseopname))
		     (docstr (concat (nth 1 prefixspec) basedocstr))
		     (fmtargs (mapcar 'symbol-value (nth 4 (nth i output-ops)))))
		(save-excursion
		  (set-buffer (marker-buffer m))
		  (goto-char m)
		  (insert-before-markers (apply 'format fmtstr fmtargs)))))))))))
