
#ifndef MACROS_H
#define MACROS_H

/* Several other C/C++ compilers, like Comeau C++, also have good gcc compatibility. Change this */
#if defined(__GNUC__) || defined(__IBMC__)
# define pure __attribute__ ((__pure__))
# define format_chk(fmt_arg) __attribute__ ((__format__ (__printf__, fmt_arg, fmt_arg + 1)))
# define _noreturn __attribute__ ((__noreturn__))
#else
# define pure
# define format_chk(fmt_arg)
# define _noreturn
#endif


#ifdef NDEBUG
# define DPRINT(args)
# define DEBUG(arg)
#else
# define DPRINT(args) printf args
# define DEBUG(arg) arg
#endif

#endif
